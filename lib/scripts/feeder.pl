#!/usr/bin/perl
#
# $Id: feeder.pl,v 1.7 2004/06/21 08:25:58 kleing Exp $
# Author: Markus Wenzel, TU Muenchen
#
# feeder.pl - feed isabelle session
#

# args

($head, $emitpid, $quit, $tail) = @ARGV;


# setup signal handlers

sub hangup { exit(0); }
$SIG{'HUP'} = "hangup";
$SIG{'INT'} = "IGNORE";


# main

#buffer lines
$| = 1;


$emitpid && (print $$, "\n");

$head && (print "$head", "\n");

if (!$quit) {
    while (<STDIN>) {
	print;
    }
}

$tail && (print "$tail", "\n");


# wait forever

close STDOUT;
sleep;
