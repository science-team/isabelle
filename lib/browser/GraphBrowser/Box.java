/***************************************************************************
  Title:      GraphBrowser/Box.java
  ID:         $Id: Box.java,v 1.3 2004/06/21 08:25:57 kleing Exp $
  Author:     Gerwin Klein, TU Muenchen
  Copyright   2003  TU Muenchen

  A box with width and height. Used instead of java.awt.Dimension for 
  batch mode.

***************************************************************************/

package GraphBrowser;

public class Box {
  public int width;
  public int height;

  public Box(int w, int h) {
    this.width = w;
    this.height = h;
  }
}
