/***************************************************************************
  Title:      GraphBrowser/AWTFontMetrics.java
  ID:         $Id: AbstractFontMetrics.java,v 1.3 2004/06/21 08:25:57 kleing Exp $
  Author:     Gerwin Klein, TU Muenchen
  Copyright   2003  TU Muenchen

  AbstractFontMetrics avoids dependency on java.awt.FontMetrics in 
  batch mode.
  
***************************************************************************/

package GraphBrowser;

public interface AbstractFontMetrics {
  public int stringWidth(String str);
  public int getAscent();
  public int getDescent();
}
