(*  Title:      HOL/Real/HahnBanach/Linearform.thy
    ID:         $Id: Linearform.thy,v 1.21 2008/01/02 14:14:12 haftmann Exp $
    Author:     Gertrud Bauer, TU Munich
*)

header {* Linearforms *}

theory Linearform imports VectorSpace begin

text {*
  A \emph{linear form} is a function on a vector space into the reals
  that is additive and multiplicative.
*}

locale linearform = var V + var f +
  constrains V :: "'a\<Colon>{minus, plus, zero, uminus} set"
  assumes add [iff]: "x \<in> V \<Longrightarrow> y \<in> V \<Longrightarrow> f (x + y) = f x + f y"
    and mult [iff]: "x \<in> V \<Longrightarrow> f (a \<cdot> x) = a * f x"

declare linearform.intro [intro?]

lemma (in linearform) neg [iff]:
  includes vectorspace
  shows "x \<in> V \<Longrightarrow> f (- x) = - f x"
proof -
  assume x: "x \<in> V"
  hence "f (- x) = f ((- 1) \<cdot> x)" by (simp add: negate_eq1)
  also from x have "... = (- 1) * (f x)" by (rule mult)
  also from x have "... = - (f x)" by simp
  finally show ?thesis .
qed

lemma (in linearform) diff [iff]:
  includes vectorspace
  shows "x \<in> V \<Longrightarrow> y \<in> V \<Longrightarrow> f (x - y) = f x - f y"
proof -
  assume x: "x \<in> V" and y: "y \<in> V"
  hence "x - y = x + - y" by (rule diff_eq1)
  also have "f ... = f x + f (- y)" by (rule add) (simp_all add: x y)
  also have "f (- y) = - f y" using `vectorspace V` y by (rule neg)
  finally show ?thesis by simp
qed

text {* Every linear form yields @{text 0} for the @{text 0} vector. *}

lemma (in linearform) zero [iff]:
  includes vectorspace
  shows "f 0 = 0"
proof -
  have "f 0 = f (0 - 0)" by simp
  also have "\<dots> = f 0 - f 0" using `vectorspace V` by (rule diff) simp_all
  also have "\<dots> = 0" by simp
  finally show ?thesis .
qed

end
