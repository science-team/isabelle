(*
    Principle ideal domains
    $Id: PID.thy,v 1.3 2006/08/03 12:58:28 ballarin Exp $
    Author: Clemens Ballarin, started 5 October 1999
*)

theory PID imports Ideal2 begin

instance pid < factorial
  apply intro_classes
   apply (rule TrueI)
  apply (erule pid_irred_imp_prime)
  done

end
