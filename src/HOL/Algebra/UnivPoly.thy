(*
  Title:     HOL/Algebra/UnivPoly.thy
  Id:        $Id: UnivPoly.thy,v 1.36 2008/05/17 21:37:07 wenzelm Exp $
  Author:    Clemens Ballarin, started 9 December 1996
  Copyright: Clemens Ballarin
*)

theory UnivPoly imports Module begin


section {* Univariate Polynomials *}

text {*
  Polynomials are formalised as modules with additional operations for
  extracting coefficients from polynomials and for obtaining monomials
  from coefficients and exponents (record @{text "up_ring"}).  The
  carrier set is a set of bounded functions from Nat to the
  coefficient domain.  Bounded means that these functions return zero
  above a certain bound (the degree).  There is a chapter on the
  formalisation of polynomials in the PhD thesis \cite{Ballarin:1999},
  which was implemented with axiomatic type classes.  This was later
  ported to Locales.
*}


subsection {* The Constructor for Univariate Polynomials *}

text {*
  Functions with finite support.
*}

locale bound =
  fixes z :: 'a
    and n :: nat
    and f :: "nat => 'a"
  assumes bound: "!!m. n < m \<Longrightarrow> f m = z"

declare bound.intro [intro!]
  and bound.bound [dest]

lemma bound_below:
  assumes bound: "bound z m f" and nonzero: "f n \<noteq> z" shows "n \<le> m"
proof (rule classical)
  assume "~ ?thesis"
  then have "m < n" by arith
  with bound have "f n = z" ..
  with nonzero show ?thesis by contradiction
qed

record ('a, 'p) up_ring = "('a, 'p) module" +
  monom :: "['a, nat] => 'p"
  coeff :: "['p, nat] => 'a"

constdefs (structure R)
  up :: "('a, 'm) ring_scheme => (nat => 'a) set"
  "up R == {f. f \<in> UNIV -> carrier R & (EX n. bound \<zero> n f)}"
  UP :: "('a, 'm) ring_scheme => ('a, nat => 'a) up_ring"
  "UP R == (|
    carrier = up R,
    mult = (%p:up R. %q:up R. %n. \<Oplus>i \<in> {..n}. p i \<otimes> q (n-i)),
    one = (%i. if i=0 then \<one> else \<zero>),
    zero = (%i. \<zero>),
    add = (%p:up R. %q:up R. %i. p i \<oplus> q i),
    smult = (%a:carrier R. %p:up R. %i. a \<otimes> p i),
    monom = (%a:carrier R. %n i. if i=n then a else \<zero>),
    coeff = (%p:up R. %n. p n) |)"

text {*
  Properties of the set of polynomials @{term up}.
*}

lemma mem_upI [intro]:
  "[| !!n. f n \<in> carrier R; EX n. bound (zero R) n f |] ==> f \<in> up R"
  by (simp add: up_def Pi_def)

lemma mem_upD [dest]:
  "f \<in> up R ==> f n \<in> carrier R"
  by (simp add: up_def Pi_def)

lemma (in cring) bound_upD [dest]:
  "f \<in> up R ==> EX n. bound \<zero> n f"
  by (simp add: up_def)

lemma (in cring) up_one_closed:
   "(%n. if n = 0 then \<one> else \<zero>) \<in> up R"
  using up_def by force

lemma (in cring) up_smult_closed:
  "[| a \<in> carrier R; p \<in> up R |] ==> (%i. a \<otimes> p i) \<in> up R"
  by force

lemma (in cring) up_add_closed:
  "[| p \<in> up R; q \<in> up R |] ==> (%i. p i \<oplus> q i) \<in> up R"
proof
  fix n
  assume "p \<in> up R" and "q \<in> up R"
  then show "p n \<oplus> q n \<in> carrier R"
    by auto
next
  assume UP: "p \<in> up R" "q \<in> up R"
  show "EX n. bound \<zero> n (%i. p i \<oplus> q i)"
  proof -
    from UP obtain n where boundn: "bound \<zero> n p" by fast
    from UP obtain m where boundm: "bound \<zero> m q" by fast
    have "bound \<zero> (max n m) (%i. p i \<oplus> q i)"
    proof
      fix i
      assume "max n m < i"
      with boundn and boundm and UP show "p i \<oplus> q i = \<zero>" by fastsimp
    qed
    then show ?thesis ..
  qed
qed

lemma (in cring) up_a_inv_closed:
  "p \<in> up R ==> (%i. \<ominus> (p i)) \<in> up R"
proof
  assume R: "p \<in> up R"
  then obtain n where "bound \<zero> n p" by auto
  then have "bound \<zero> n (%i. \<ominus> p i)" by auto
  then show "EX n. bound \<zero> n (%i. \<ominus> p i)" by auto
qed auto

lemma (in cring) up_mult_closed:
  "[| p \<in> up R; q \<in> up R |] ==>
  (%n. \<Oplus>i \<in> {..n}. p i \<otimes> q (n-i)) \<in> up R"
proof
  fix n
  assume "p \<in> up R" "q \<in> up R"
  then show "(\<Oplus>i \<in> {..n}. p i \<otimes> q (n-i)) \<in> carrier R"
    by (simp add: mem_upD  funcsetI)
next
  assume UP: "p \<in> up R" "q \<in> up R"
  show "EX n. bound \<zero> n (%n. \<Oplus>i \<in> {..n}. p i \<otimes> q (n-i))"
  proof -
    from UP obtain n where boundn: "bound \<zero> n p" by fast
    from UP obtain m where boundm: "bound \<zero> m q" by fast
    have "bound \<zero> (n + m) (%n. \<Oplus>i \<in> {..n}. p i \<otimes> q (n - i))"
    proof
      fix k assume bound: "n + m < k"
      {
        fix i
        have "p i \<otimes> q (k-i) = \<zero>"
        proof (cases "n < i")
          case True
          with boundn have "p i = \<zero>" by auto
          moreover from UP have "q (k-i) \<in> carrier R" by auto
          ultimately show ?thesis by simp
        next
          case False
          with bound have "m < k-i" by arith
          with boundm have "q (k-i) = \<zero>" by auto
          moreover from UP have "p i \<in> carrier R" by auto
          ultimately show ?thesis by simp
        qed
      }
      then show "(\<Oplus>i \<in> {..k}. p i \<otimes> q (k-i)) = \<zero>"
        by (simp add: Pi_def)
    qed
    then show ?thesis by fast
  qed
qed


subsection {* Effect of Operations on Coefficients *}

locale UP =
  fixes R (structure) and P (structure)
  defines P_def: "P == UP R"

locale UP_cring = UP + cring R

locale UP_domain = UP_cring + "domain" R

text {*
  Temporarily declare @{thm [locale=UP] P_def} as simp rule.
*}

declare (in UP) P_def [simp]

lemma (in UP_cring) coeff_monom [simp]:
  "a \<in> carrier R ==>
  coeff P (monom P a m) n = (if m=n then a else \<zero>)"
proof -
  assume R: "a \<in> carrier R"
  then have "(%n. if n = m then a else \<zero>) \<in> up R"
    using up_def by force
  with R show ?thesis by (simp add: UP_def)
qed

lemma (in UP_cring) coeff_zero [simp]:
  "coeff P \<zero>\<^bsub>P\<^esub> n = \<zero>"
  by (auto simp add: UP_def)

lemma (in UP_cring) coeff_one [simp]:
  "coeff P \<one>\<^bsub>P\<^esub> n = (if n=0 then \<one> else \<zero>)"
  using up_one_closed by (simp add: UP_def)

lemma (in UP_cring) coeff_smult [simp]:
  "[| a \<in> carrier R; p \<in> carrier P |] ==>
  coeff P (a \<odot>\<^bsub>P\<^esub> p) n = a \<otimes> coeff P p n"
  by (simp add: UP_def up_smult_closed)

lemma (in UP_cring) coeff_add [simp]:
  "[| p \<in> carrier P; q \<in> carrier P |] ==>
  coeff P (p \<oplus>\<^bsub>P\<^esub> q) n = coeff P p n \<oplus> coeff P q n"
  by (simp add: UP_def up_add_closed)

lemma (in UP_cring) coeff_mult [simp]:
  "[| p \<in> carrier P; q \<in> carrier P |] ==>
  coeff P (p \<otimes>\<^bsub>P\<^esub> q) n = (\<Oplus>i \<in> {..n}. coeff P p i \<otimes> coeff P q (n-i))"
  by (simp add: UP_def up_mult_closed)

lemma (in UP) up_eqI:
  assumes prem: "!!n. coeff P p n = coeff P q n"
    and R: "p \<in> carrier P" "q \<in> carrier P"
  shows "p = q"
proof
  fix x
  from prem and R show "p x = q x" by (simp add: UP_def)
qed


subsection {* Polynomials Form a Commutative Ring. *}

text {* Operations are closed over @{term P}. *}

lemma (in UP_cring) UP_mult_closed [simp]:
  "[| p \<in> carrier P; q \<in> carrier P |] ==> p \<otimes>\<^bsub>P\<^esub> q \<in> carrier P"
  by (simp add: UP_def up_mult_closed)

lemma (in UP_cring) UP_one_closed [simp]:
  "\<one>\<^bsub>P\<^esub> \<in> carrier P"
  by (simp add: UP_def up_one_closed)

lemma (in UP_cring) UP_zero_closed [intro, simp]:
  "\<zero>\<^bsub>P\<^esub> \<in> carrier P"
  by (auto simp add: UP_def)

lemma (in UP_cring) UP_a_closed [intro, simp]:
  "[| p \<in> carrier P; q \<in> carrier P |] ==> p \<oplus>\<^bsub>P\<^esub> q \<in> carrier P"
  by (simp add: UP_def up_add_closed)

lemma (in UP_cring) monom_closed [simp]:
  "a \<in> carrier R ==> monom P a n \<in> carrier P"
  by (auto simp add: UP_def up_def Pi_def)

lemma (in UP_cring) UP_smult_closed [simp]:
  "[| a \<in> carrier R; p \<in> carrier P |] ==> a \<odot>\<^bsub>P\<^esub> p \<in> carrier P"
  by (simp add: UP_def up_smult_closed)

lemma (in UP) coeff_closed [simp]:
  "p \<in> carrier P ==> coeff P p n \<in> carrier R"
  by (auto simp add: UP_def)

declare (in UP) P_def [simp del]

text {* Algebraic ring properties *}

lemma (in UP_cring) UP_a_assoc:
  assumes R: "p \<in> carrier P" "q \<in> carrier P" "r \<in> carrier P"
  shows "(p \<oplus>\<^bsub>P\<^esub> q) \<oplus>\<^bsub>P\<^esub> r = p \<oplus>\<^bsub>P\<^esub> (q \<oplus>\<^bsub>P\<^esub> r)"
  by (rule up_eqI, simp add: a_assoc R, simp_all add: R)

lemma (in UP_cring) UP_l_zero [simp]:
  assumes R: "p \<in> carrier P"
  shows "\<zero>\<^bsub>P\<^esub> \<oplus>\<^bsub>P\<^esub> p = p"
  by (rule up_eqI, simp_all add: R)

lemma (in UP_cring) UP_l_neg_ex:
  assumes R: "p \<in> carrier P"
  shows "EX q : carrier P. q \<oplus>\<^bsub>P\<^esub> p = \<zero>\<^bsub>P\<^esub>"
proof -
  let ?q = "%i. \<ominus> (p i)"
  from R have closed: "?q \<in> carrier P"
    by (simp add: UP_def P_def up_a_inv_closed)
  from R have coeff: "!!n. coeff P ?q n = \<ominus> (coeff P p n)"
    by (simp add: UP_def P_def up_a_inv_closed)
  show ?thesis
  proof
    show "?q \<oplus>\<^bsub>P\<^esub> p = \<zero>\<^bsub>P\<^esub>"
      by (auto intro!: up_eqI simp add: R closed coeff R.l_neg)
  qed (rule closed)
qed

lemma (in UP_cring) UP_a_comm:
  assumes R: "p \<in> carrier P" "q \<in> carrier P"
  shows "p \<oplus>\<^bsub>P\<^esub> q = q \<oplus>\<^bsub>P\<^esub> p"
  by (rule up_eqI, simp add: a_comm R, simp_all add: R)

lemma (in UP_cring) UP_m_assoc:
  assumes R: "p \<in> carrier P" "q \<in> carrier P" "r \<in> carrier P"
  shows "(p \<otimes>\<^bsub>P\<^esub> q) \<otimes>\<^bsub>P\<^esub> r = p \<otimes>\<^bsub>P\<^esub> (q \<otimes>\<^bsub>P\<^esub> r)"
proof (rule up_eqI)
  fix n
  {
    fix k and a b c :: "nat=>'a"
    assume R: "a \<in> UNIV -> carrier R" "b \<in> UNIV -> carrier R"
      "c \<in> UNIV -> carrier R"
    then have "k <= n ==>
      (\<Oplus>j \<in> {..k}. (\<Oplus>i \<in> {..j}. a i \<otimes> b (j-i)) \<otimes> c (n-j)) =
      (\<Oplus>j \<in> {..k}. a j \<otimes> (\<Oplus>i \<in> {..k-j}. b i \<otimes> c (n-j-i)))"
      (is "_ \<Longrightarrow> ?eq k")
    proof (induct k)
      case 0 then show ?case by (simp add: Pi_def m_assoc)
    next
      case (Suc k)
      then have "k <= n" by arith
      from this R have "?eq k" by (rule Suc)
      with R show ?case
        by (simp cong: finsum_cong
             add: Suc_diff_le Pi_def l_distr r_distr m_assoc)
          (simp cong: finsum_cong add: Pi_def a_ac finsum_ldistr m_assoc)
    qed
  }
  with R show "coeff P ((p \<otimes>\<^bsub>P\<^esub> q) \<otimes>\<^bsub>P\<^esub> r) n = coeff P (p \<otimes>\<^bsub>P\<^esub> (q \<otimes>\<^bsub>P\<^esub> r)) n"
    by (simp add: Pi_def)
qed (simp_all add: R)

lemma (in UP_cring) UP_l_one [simp]:
  assumes R: "p \<in> carrier P"
  shows "\<one>\<^bsub>P\<^esub> \<otimes>\<^bsub>P\<^esub> p = p"
proof (rule up_eqI)
  fix n
  show "coeff P (\<one>\<^bsub>P\<^esub> \<otimes>\<^bsub>P\<^esub> p) n = coeff P p n"
  proof (cases n)
    case 0 with R show ?thesis by simp
  next
    case Suc with R show ?thesis
      by (simp del: finsum_Suc add: finsum_Suc2 Pi_def)
  qed
qed (simp_all add: R)

lemma (in UP_cring) UP_l_distr:
  assumes R: "p \<in> carrier P" "q \<in> carrier P" "r \<in> carrier P"
  shows "(p \<oplus>\<^bsub>P\<^esub> q) \<otimes>\<^bsub>P\<^esub> r = (p \<otimes>\<^bsub>P\<^esub> r) \<oplus>\<^bsub>P\<^esub> (q \<otimes>\<^bsub>P\<^esub> r)"
  by (rule up_eqI) (simp add: l_distr R Pi_def, simp_all add: R)

lemma (in UP_cring) UP_m_comm:
  assumes R: "p \<in> carrier P" "q \<in> carrier P"
  shows "p \<otimes>\<^bsub>P\<^esub> q = q \<otimes>\<^bsub>P\<^esub> p"
proof (rule up_eqI)
  fix n
  {
    fix k and a b :: "nat=>'a"
    assume R: "a \<in> UNIV -> carrier R" "b \<in> UNIV -> carrier R"
    then have "k <= n ==>
      (\<Oplus>i \<in> {..k}. a i \<otimes> b (n-i)) =
      (\<Oplus>i \<in> {..k}. a (k-i) \<otimes> b (i+n-k))"
      (is "_ \<Longrightarrow> ?eq k")
    proof (induct k)
      case 0 then show ?case by (simp add: Pi_def)
    next
      case (Suc k) then show ?case
        by (subst (2) finsum_Suc2) (simp add: Pi_def a_comm)+
    qed
  }
  note l = this
  from R show "coeff P (p \<otimes>\<^bsub>P\<^esub> q) n =  coeff P (q \<otimes>\<^bsub>P\<^esub> p) n"
    apply (simp add: Pi_def)
    apply (subst l)
    apply (auto simp add: Pi_def)
    apply (simp add: m_comm)
    done
qed (simp_all add: R)

theorem (in UP_cring) UP_cring:
  "cring P"
  by (auto intro!: cringI abelian_groupI comm_monoidI UP_a_assoc UP_l_zero
    UP_l_neg_ex UP_a_comm UP_m_assoc UP_l_one UP_m_comm UP_l_distr)

lemma (in UP_cring) UP_ring:
  (* preliminary,
     we want "UP_ring R P ==> ring P", not "UP_cring R P ==> ring P" *)
  "ring P"
  by (auto intro: ring.intro cring.axioms UP_cring)

lemma (in UP_cring) UP_a_inv_closed [intro, simp]:
  "p \<in> carrier P ==> \<ominus>\<^bsub>P\<^esub> p \<in> carrier P"
  by (rule abelian_group.a_inv_closed
    [OF ring.is_abelian_group [OF UP_ring]])

lemma (in UP_cring) coeff_a_inv [simp]:
  assumes R: "p \<in> carrier P"
  shows "coeff P (\<ominus>\<^bsub>P\<^esub> p) n = \<ominus> (coeff P p n)"
proof -
  from R coeff_closed UP_a_inv_closed have
    "coeff P (\<ominus>\<^bsub>P\<^esub> p) n = \<ominus> coeff P p n \<oplus> (coeff P p n \<oplus> coeff P (\<ominus>\<^bsub>P\<^esub> p) n)"
    by algebra
  also from R have "... =  \<ominus> (coeff P p n)"
    by (simp del: coeff_add add: coeff_add [THEN sym]
      abelian_group.r_neg [OF ring.is_abelian_group [OF UP_ring]])
  finally show ?thesis .
qed

text {*
  Interpretation of lemmas from @{term cring}.  Saves lifting 43
  lemmas manually.
*}

interpretation UP_cring < cring P
  by intro_locales
    (rule cring.axioms ring.axioms abelian_group.axioms comm_monoid.axioms UP_cring)+


subsection {* Polynomials Form an Algebra *}

lemma (in UP_cring) UP_smult_l_distr:
  "[| a \<in> carrier R; b \<in> carrier R; p \<in> carrier P |] ==>
  (a \<oplus> b) \<odot>\<^bsub>P\<^esub> p = a \<odot>\<^bsub>P\<^esub> p \<oplus>\<^bsub>P\<^esub> b \<odot>\<^bsub>P\<^esub> p"
  by (rule up_eqI) (simp_all add: R.l_distr)

lemma (in UP_cring) UP_smult_r_distr:
  "[| a \<in> carrier R; p \<in> carrier P; q \<in> carrier P |] ==>
  a \<odot>\<^bsub>P\<^esub> (p \<oplus>\<^bsub>P\<^esub> q) = a \<odot>\<^bsub>P\<^esub> p \<oplus>\<^bsub>P\<^esub> a \<odot>\<^bsub>P\<^esub> q"
  by (rule up_eqI) (simp_all add: R.r_distr)

lemma (in UP_cring) UP_smult_assoc1:
      "[| a \<in> carrier R; b \<in> carrier R; p \<in> carrier P |] ==>
      (a \<otimes> b) \<odot>\<^bsub>P\<^esub> p = a \<odot>\<^bsub>P\<^esub> (b \<odot>\<^bsub>P\<^esub> p)"
  by (rule up_eqI) (simp_all add: R.m_assoc)

lemma (in UP_cring) UP_smult_one [simp]:
      "p \<in> carrier P ==> \<one> \<odot>\<^bsub>P\<^esub> p = p"
  by (rule up_eqI) simp_all

lemma (in UP_cring) UP_smult_assoc2:
  "[| a \<in> carrier R; p \<in> carrier P; q \<in> carrier P |] ==>
  (a \<odot>\<^bsub>P\<^esub> p) \<otimes>\<^bsub>P\<^esub> q = a \<odot>\<^bsub>P\<^esub> (p \<otimes>\<^bsub>P\<^esub> q)"
  by (rule up_eqI) (simp_all add: R.finsum_rdistr R.m_assoc Pi_def)

text {*
  Interpretation of lemmas from @{term algebra}.
*}

lemma (in cring) cring:
  "cring R"
  by (fast intro: cring.intro prems)

lemma (in UP_cring) UP_algebra:
  "algebra R P"
  by (auto intro: algebraI R.cring UP_cring UP_smult_l_distr UP_smult_r_distr
    UP_smult_assoc1 UP_smult_assoc2)

interpretation UP_cring < algebra R P
  by intro_locales
    (rule module.axioms algebra.axioms UP_algebra)+


subsection {* Further Lemmas Involving Monomials *}

lemma (in UP_cring) monom_zero [simp]:
  "monom P \<zero> n = \<zero>\<^bsub>P\<^esub>"
  by (simp add: UP_def P_def)

lemma (in UP_cring) monom_mult_is_smult:
  assumes R: "a \<in> carrier R" "p \<in> carrier P"
  shows "monom P a 0 \<otimes>\<^bsub>P\<^esub> p = a \<odot>\<^bsub>P\<^esub> p"
proof (rule up_eqI)
  fix n
  have "coeff P (p \<otimes>\<^bsub>P\<^esub> monom P a 0) n = coeff P (a \<odot>\<^bsub>P\<^esub> p) n"
  proof (cases n)
    case 0 with R show ?thesis by (simp add: R.m_comm)
  next
    case Suc with R show ?thesis
      by (simp cong: R.finsum_cong add: R.r_null Pi_def)
        (simp add: R.m_comm)
  qed
  with R show "coeff P (monom P a 0 \<otimes>\<^bsub>P\<^esub> p) n = coeff P (a \<odot>\<^bsub>P\<^esub> p) n"
    by (simp add: UP_m_comm)
qed (simp_all add: R)

lemma (in UP_cring) monom_add [simp]:
  "[| a \<in> carrier R; b \<in> carrier R |] ==>
  monom P (a \<oplus> b) n = monom P a n \<oplus>\<^bsub>P\<^esub> monom P b n"
  by (rule up_eqI) simp_all

lemma (in UP_cring) monom_one_Suc:
  "monom P \<one> (Suc n) = monom P \<one> n \<otimes>\<^bsub>P\<^esub> monom P \<one> 1"
proof (rule up_eqI)
  fix k
  show "coeff P (monom P \<one> (Suc n)) k = coeff P (monom P \<one> n \<otimes>\<^bsub>P\<^esub> monom P \<one> 1) k"
  proof (cases "k = Suc n")
    case True show ?thesis
    proof -
      fix m
      from True have less_add_diff:
        "!!i. [| n < i; i <= n + m |] ==> n + m - i < m" by arith
      from True have "coeff P (monom P \<one> (Suc n)) k = \<one>" by simp
      also from True
      have "... = (\<Oplus>i \<in> {..<n} \<union> {n}. coeff P (monom P \<one> n) i \<otimes>
        coeff P (monom P \<one> 1) (k - i))"
        by (simp cong: R.finsum_cong add: Pi_def)
      also have "... = (\<Oplus>i \<in>  {..n}. coeff P (monom P \<one> n) i \<otimes>
        coeff P (monom P \<one> 1) (k - i))"
        by (simp only: ivl_disj_un_singleton)
      also from True
      have "... = (\<Oplus>i \<in> {..n} \<union> {n<..k}. coeff P (monom P \<one> n) i \<otimes>
        coeff P (monom P \<one> 1) (k - i))"
        by (simp cong: R.finsum_cong add: R.finsum_Un_disjoint ivl_disj_int_one
          order_less_imp_not_eq Pi_def)
      also from True have "... = coeff P (monom P \<one> n \<otimes>\<^bsub>P\<^esub> monom P \<one> 1) k"
        by (simp add: ivl_disj_un_one)
      finally show ?thesis .
    qed
  next
    case False
    note neq = False
    let ?s =
      "\<lambda>i. (if n = i then \<one> else \<zero>) \<otimes> (if Suc 0 = k - i then \<one> else \<zero>)"
    from neq have "coeff P (monom P \<one> (Suc n)) k = \<zero>" by simp
    also have "... = (\<Oplus>i \<in> {..k}. ?s i)"
    proof -
      have f1: "(\<Oplus>i \<in> {..<n}. ?s i) = \<zero>"
        by (simp cong: R.finsum_cong add: Pi_def)
      from neq have f2: "(\<Oplus>i \<in> {n}. ?s i) = \<zero>"
        by (simp cong: R.finsum_cong add: Pi_def) arith
      have f3: "n < k ==> (\<Oplus>i \<in> {n<..k}. ?s i) = \<zero>"
        by (simp cong: R.finsum_cong add: order_less_imp_not_eq Pi_def)
      show ?thesis
      proof (cases "k < n")
        case True then show ?thesis by (simp cong: R.finsum_cong add: Pi_def)
      next
        case False then have n_le_k: "n <= k" by arith
        show ?thesis
        proof (cases "n = k")
          case True
          then have "\<zero> = (\<Oplus>i \<in> {..<n} \<union> {n}. ?s i)"
            by (simp cong: R.finsum_cong add: ivl_disj_int_singleton Pi_def)
          also from True have "... = (\<Oplus>i \<in> {..k}. ?s i)"
            by (simp only: ivl_disj_un_singleton)
          finally show ?thesis .
        next
          case False with n_le_k have n_less_k: "n < k" by arith
          with neq have "\<zero> = (\<Oplus>i \<in> {..<n} \<union> {n}. ?s i)"
            by (simp add: R.finsum_Un_disjoint f1 f2
              ivl_disj_int_singleton Pi_def del: Un_insert_right)
          also have "... = (\<Oplus>i \<in> {..n}. ?s i)"
            by (simp only: ivl_disj_un_singleton)
          also from n_less_k neq have "... = (\<Oplus>i \<in> {..n} \<union> {n<..k}. ?s i)"
            by (simp add: R.finsum_Un_disjoint f3 ivl_disj_int_one Pi_def)
          also from n_less_k have "... = (\<Oplus>i \<in> {..k}. ?s i)"
            by (simp only: ivl_disj_un_one)
          finally show ?thesis .
        qed
      qed
    qed
    also have "... = coeff P (monom P \<one> n \<otimes>\<^bsub>P\<^esub> monom P \<one> 1) k" by simp
    finally show ?thesis .
  qed
qed (simp_all)

lemma (in UP_cring) monom_mult_smult:
  "[| a \<in> carrier R; b \<in> carrier R |] ==> monom P (a \<otimes> b) n = a \<odot>\<^bsub>P\<^esub> monom P b n"
  by (rule up_eqI) simp_all

lemma (in UP_cring) monom_one [simp]:
  "monom P \<one> 0 = \<one>\<^bsub>P\<^esub>"
  by (rule up_eqI) simp_all

lemma (in UP_cring) monom_one_mult:
  "monom P \<one> (n + m) = monom P \<one> n \<otimes>\<^bsub>P\<^esub> monom P \<one> m"
proof (induct n)
  case 0 show ?case by simp
next
  case Suc then show ?case
    by (simp only: add_Suc monom_one_Suc) (simp add: P.m_ac)
qed

lemma (in UP_cring) monom_mult [simp]:
  assumes R: "a \<in> carrier R" "b \<in> carrier R"
  shows "monom P (a \<otimes> b) (n + m) = monom P a n \<otimes>\<^bsub>P\<^esub> monom P b m"
proof -
  from R have "monom P (a \<otimes> b) (n + m) = monom P (a \<otimes> b \<otimes> \<one>) (n + m)" by simp
  also from R have "... = a \<otimes> b \<odot>\<^bsub>P\<^esub> monom P \<one> (n + m)"
    by (simp add: monom_mult_smult del: R.r_one)
  also have "... = a \<otimes> b \<odot>\<^bsub>P\<^esub> (monom P \<one> n \<otimes>\<^bsub>P\<^esub> monom P \<one> m)"
    by (simp only: monom_one_mult)
  also from R have "... = a \<odot>\<^bsub>P\<^esub> (b \<odot>\<^bsub>P\<^esub> (monom P \<one> n \<otimes>\<^bsub>P\<^esub> monom P \<one> m))"
    by (simp add: UP_smult_assoc1)
  also from R have "... = a \<odot>\<^bsub>P\<^esub> (b \<odot>\<^bsub>P\<^esub> (monom P \<one> m \<otimes>\<^bsub>P\<^esub> monom P \<one> n))"
    by (simp add: P.m_comm)
  also from R have "... = a \<odot>\<^bsub>P\<^esub> ((b \<odot>\<^bsub>P\<^esub> monom P \<one> m) \<otimes>\<^bsub>P\<^esub> monom P \<one> n)"
    by (simp add: UP_smult_assoc2)
  also from R have "... = a \<odot>\<^bsub>P\<^esub> (monom P \<one> n \<otimes>\<^bsub>P\<^esub> (b \<odot>\<^bsub>P\<^esub> monom P \<one> m))"
    by (simp add: P.m_comm)
  also from R have "... = (a \<odot>\<^bsub>P\<^esub> monom P \<one> n) \<otimes>\<^bsub>P\<^esub> (b \<odot>\<^bsub>P\<^esub> monom P \<one> m)"
    by (simp add: UP_smult_assoc2)
  also from R have "... = monom P (a \<otimes> \<one>) n \<otimes>\<^bsub>P\<^esub> monom P (b \<otimes> \<one>) m"
    by (simp add: monom_mult_smult del: R.r_one)
  also from R have "... = monom P a n \<otimes>\<^bsub>P\<^esub> monom P b m" by simp
  finally show ?thesis .
qed

lemma (in UP_cring) monom_a_inv [simp]:
  "a \<in> carrier R ==> monom P (\<ominus> a) n = \<ominus>\<^bsub>P\<^esub> monom P a n"
  by (rule up_eqI) simp_all

lemma (in UP_cring) monom_inj:
  "inj_on (%a. monom P a n) (carrier R)"
proof (rule inj_onI)
  fix x y
  assume R: "x \<in> carrier R" "y \<in> carrier R" and eq: "monom P x n = monom P y n"
  then have "coeff P (monom P x n) n = coeff P (monom P y n) n" by simp
  with R show "x = y" by simp
qed


subsection {* The Degree Function *}

constdefs (structure R)
  deg :: "[('a, 'm) ring_scheme, nat => 'a] => nat"
  "deg R p == LEAST n. bound \<zero> n (coeff (UP R) p)"

lemma (in UP_cring) deg_aboveI:
  "[| (!!m. n < m ==> coeff P p m = \<zero>); p \<in> carrier P |] ==> deg R p <= n"
  by (unfold deg_def P_def) (fast intro: Least_le)

(*
lemma coeff_bound_ex: "EX n. bound n (coeff p)"
proof -
  have "(%n. coeff p n) : UP" by (simp add: coeff_def Rep_UP)
  then obtain n where "bound n (coeff p)" by (unfold UP_def) fast
  then show ?thesis ..
qed

lemma bound_coeff_obtain:
  assumes prem: "(!!n. bound n (coeff p) ==> P)" shows "P"
proof -
  have "(%n. coeff p n) : UP" by (simp add: coeff_def Rep_UP)
  then obtain n where "bound n (coeff p)" by (unfold UP_def) fast
  with prem show P .
qed
*)

lemma (in UP_cring) deg_aboveD:
  assumes "deg R p < m" and "p \<in> carrier P"
  shows "coeff P p m = \<zero>"
proof -
  from `p \<in> carrier P` obtain n where "bound \<zero> n (coeff P p)"
    by (auto simp add: UP_def P_def)
  then have "bound \<zero> (deg R p) (coeff P p)"
    by (auto simp: deg_def P_def dest: LeastI)
  from this and `deg R p < m` show ?thesis ..
qed

lemma (in UP_cring) deg_belowI:
  assumes non_zero: "n ~= 0 ==> coeff P p n ~= \<zero>"
    and R: "p \<in> carrier P"
  shows "n <= deg R p"
-- {* Logically, this is a slightly stronger version of
   @{thm [source] deg_aboveD} *}
proof (cases "n=0")
  case True then show ?thesis by simp
next
  case False then have "coeff P p n ~= \<zero>" by (rule non_zero)
  then have "~ deg R p < n" by (fast dest: deg_aboveD intro: R)
  then show ?thesis by arith
qed

lemma (in UP_cring) lcoeff_nonzero_deg:
  assumes deg: "deg R p ~= 0" and R: "p \<in> carrier P"
  shows "coeff P p (deg R p) ~= \<zero>"
proof -
  from R obtain m where "deg R p <= m" and m_coeff: "coeff P p m ~= \<zero>"
  proof -
    have minus: "!!(n::nat) m. n ~= 0 ==> (n - Suc 0 < m) = (n <= m)"
      by arith
(* TODO: why does simplification below not work with "1" *)
    from deg have "deg R p - 1 < (LEAST n. bound \<zero> n (coeff P p))"
      by (unfold deg_def P_def) arith
    then have "~ bound \<zero> (deg R p - 1) (coeff P p)" by (rule not_less_Least)
    then have "EX m. deg R p - 1 < m & coeff P p m ~= \<zero>"
      by (unfold bound_def) fast
    then have "EX m. deg R p <= m & coeff P p m ~= \<zero>" by (simp add: deg minus)
    then show ?thesis by (auto intro: that)
  qed
  with deg_belowI R have "deg R p = m" by fastsimp
  with m_coeff show ?thesis by simp
qed

lemma (in UP_cring) lcoeff_nonzero_nonzero:
  assumes deg: "deg R p = 0" and nonzero: "p ~= \<zero>\<^bsub>P\<^esub>" and R: "p \<in> carrier P"
  shows "coeff P p 0 ~= \<zero>"
proof -
  have "EX m. coeff P p m ~= \<zero>"
  proof (rule classical)
    assume "~ ?thesis"
    with R have "p = \<zero>\<^bsub>P\<^esub>" by (auto intro: up_eqI)
    with nonzero show ?thesis by contradiction
  qed
  then obtain m where coeff: "coeff P p m ~= \<zero>" ..
  from this and R have "m <= deg R p" by (rule deg_belowI)
  then have "m = 0" by (simp add: deg)
  with coeff show ?thesis by simp
qed

lemma (in UP_cring) lcoeff_nonzero:
  assumes neq: "p ~= \<zero>\<^bsub>P\<^esub>" and R: "p \<in> carrier P"
  shows "coeff P p (deg R p) ~= \<zero>"
proof (cases "deg R p = 0")
  case True with neq R show ?thesis by (simp add: lcoeff_nonzero_nonzero)
next
  case False with neq R show ?thesis by (simp add: lcoeff_nonzero_deg)
qed

lemma (in UP_cring) deg_eqI:
  "[| !!m. n < m ==> coeff P p m = \<zero>;
      !!n. n ~= 0 ==> coeff P p n ~= \<zero>; p \<in> carrier P |] ==> deg R p = n"
by (fast intro: le_anti_sym deg_aboveI deg_belowI)

text {* Degree and polynomial operations *}

lemma (in UP_cring) deg_add [simp]:
  assumes R: "p \<in> carrier P" "q \<in> carrier P"
  shows "deg R (p \<oplus>\<^bsub>P\<^esub> q) <= max (deg R p) (deg R q)"
proof (cases "deg R p <= deg R q")
  case True show ?thesis
    by (rule deg_aboveI) (simp_all add: True R deg_aboveD)
next
  case False show ?thesis
    by (rule deg_aboveI) (simp_all add: False R deg_aboveD)
qed

lemma (in UP_cring) deg_monom_le:
  "a \<in> carrier R ==> deg R (monom P a n) <= n"
  by (intro deg_aboveI) simp_all

lemma (in UP_cring) deg_monom [simp]:
  "[| a ~= \<zero>; a \<in> carrier R |] ==> deg R (monom P a n) = n"
  by (fastsimp intro: le_anti_sym deg_aboveI deg_belowI)

lemma (in UP_cring) deg_const [simp]:
  assumes R: "a \<in> carrier R" shows "deg R (monom P a 0) = 0"
proof (rule le_anti_sym)
  show "deg R (monom P a 0) <= 0" by (rule deg_aboveI) (simp_all add: R)
next
  show "0 <= deg R (monom P a 0)" by (rule deg_belowI) (simp_all add: R)
qed

lemma (in UP_cring) deg_zero [simp]:
  "deg R \<zero>\<^bsub>P\<^esub> = 0"
proof (rule le_anti_sym)
  show "deg R \<zero>\<^bsub>P\<^esub> <= 0" by (rule deg_aboveI) simp_all
next
  show "0 <= deg R \<zero>\<^bsub>P\<^esub>" by (rule deg_belowI) simp_all
qed

lemma (in UP_cring) deg_one [simp]:
  "deg R \<one>\<^bsub>P\<^esub> = 0"
proof (rule le_anti_sym)
  show "deg R \<one>\<^bsub>P\<^esub> <= 0" by (rule deg_aboveI) simp_all
next
  show "0 <= deg R \<one>\<^bsub>P\<^esub>" by (rule deg_belowI) simp_all
qed

lemma (in UP_cring) deg_uminus [simp]:
  assumes R: "p \<in> carrier P" shows "deg R (\<ominus>\<^bsub>P\<^esub> p) = deg R p"
proof (rule le_anti_sym)
  show "deg R (\<ominus>\<^bsub>P\<^esub> p) <= deg R p" by (simp add: deg_aboveI deg_aboveD R)
next
  show "deg R p <= deg R (\<ominus>\<^bsub>P\<^esub> p)"
    by (simp add: deg_belowI lcoeff_nonzero_deg
      inj_on_iff [OF R.a_inv_inj, of _ "\<zero>", simplified] R)
qed

lemma (in UP_domain) deg_smult_ring:
  "[| a \<in> carrier R; p \<in> carrier P |] ==>
  deg R (a \<odot>\<^bsub>P\<^esub> p) <= (if a = \<zero> then 0 else deg R p)"
  by (cases "a = \<zero>") (simp add: deg_aboveI deg_aboveD)+

lemma (in UP_domain) deg_smult [simp]:
  assumes R: "a \<in> carrier R" "p \<in> carrier P"
  shows "deg R (a \<odot>\<^bsub>P\<^esub> p) = (if a = \<zero> then 0 else deg R p)"
proof (rule le_anti_sym)
  show "deg R (a \<odot>\<^bsub>P\<^esub> p) <= (if a = \<zero> then 0 else deg R p)"
    using R by (rule deg_smult_ring)
next
  show "(if a = \<zero> then 0 else deg R p) <= deg R (a \<odot>\<^bsub>P\<^esub> p)"
  proof (cases "a = \<zero>")
  qed (simp, simp add: deg_belowI lcoeff_nonzero_deg integral_iff R)
qed

lemma (in UP_cring) deg_mult_cring:
  assumes R: "p \<in> carrier P" "q \<in> carrier P"
  shows "deg R (p \<otimes>\<^bsub>P\<^esub> q) <= deg R p + deg R q"
proof (rule deg_aboveI)
  fix m
  assume boundm: "deg R p + deg R q < m"
  {
    fix k i
    assume boundk: "deg R p + deg R q < k"
    then have "coeff P p i \<otimes> coeff P q (k - i) = \<zero>"
    proof (cases "deg R p < i")
      case True then show ?thesis by (simp add: deg_aboveD R)
    next
      case False with boundk have "deg R q < k - i" by arith
      then show ?thesis by (simp add: deg_aboveD R)
    qed
  }
  with boundm R show "coeff P (p \<otimes>\<^bsub>P\<^esub> q) m = \<zero>" by simp
qed (simp add: R)

lemma (in UP_domain) deg_mult [simp]:
  "[| p ~= \<zero>\<^bsub>P\<^esub>; q ~= \<zero>\<^bsub>P\<^esub>; p \<in> carrier P; q \<in> carrier P |] ==>
  deg R (p \<otimes>\<^bsub>P\<^esub> q) = deg R p + deg R q"
proof (rule le_anti_sym)
  assume "p \<in> carrier P" " q \<in> carrier P"
  then show "deg R (p \<otimes>\<^bsub>P\<^esub> q) <= deg R p + deg R q" by (rule deg_mult_cring)
next
  let ?s = "(%i. coeff P p i \<otimes> coeff P q (deg R p + deg R q - i))"
  assume R: "p \<in> carrier P" "q \<in> carrier P" and nz: "p ~= \<zero>\<^bsub>P\<^esub>" "q ~= \<zero>\<^bsub>P\<^esub>"
  have less_add_diff: "!!(k::nat) n m. k < n ==> m < n + m - k" by arith
  show "deg R p + deg R q <= deg R (p \<otimes>\<^bsub>P\<^esub> q)"
  proof (rule deg_belowI, simp add: R)
    have "(\<Oplus>i \<in> {.. deg R p + deg R q}. ?s i)
      = (\<Oplus>i \<in> {..< deg R p} \<union> {deg R p .. deg R p + deg R q}. ?s i)"
      by (simp only: ivl_disj_un_one)
    also have "... = (\<Oplus>i \<in> {deg R p .. deg R p + deg R q}. ?s i)"
      by (simp cong: R.finsum_cong add: R.finsum_Un_disjoint ivl_disj_int_one
        deg_aboveD less_add_diff R Pi_def)
    also have "...= (\<Oplus>i \<in> {deg R p} \<union> {deg R p <.. deg R p + deg R q}. ?s i)"
      by (simp only: ivl_disj_un_singleton)
    also have "... = coeff P p (deg R p) \<otimes> coeff P q (deg R q)"
      by (simp cong: R.finsum_cong
	add: ivl_disj_int_singleton deg_aboveD R Pi_def)
    finally have "(\<Oplus>i \<in> {.. deg R p + deg R q}. ?s i)
      = coeff P p (deg R p) \<otimes> coeff P q (deg R q)" .
    with nz show "(\<Oplus>i \<in> {.. deg R p + deg R q}. ?s i) ~= \<zero>"
      by (simp add: integral_iff lcoeff_nonzero R)
    qed (simp add: R)
  qed

lemma (in UP_cring) coeff_finsum:
  assumes fin: "finite A"
  shows "p \<in> A -> carrier P ==>
    coeff P (finsum P p A) k = (\<Oplus>i \<in> A. coeff P (p i) k)"
  using fin by induct (auto simp: Pi_def)

lemma (in UP_cring) up_repr:
  assumes R: "p \<in> carrier P"
  shows "(\<Oplus>\<^bsub>P\<^esub> i \<in> {..deg R p}. monom P (coeff P p i) i) = p"
proof (rule up_eqI)
  let ?s = "(%i. monom P (coeff P p i) i)"
  fix k
  from R have RR: "!!i. (if i = k then coeff P p i else \<zero>) \<in> carrier R"
    by simp
  show "coeff P (\<Oplus>\<^bsub>P\<^esub> i \<in> {..deg R p}. ?s i) k = coeff P p k"
  proof (cases "k <= deg R p")
    case True
    hence "coeff P (\<Oplus>\<^bsub>P\<^esub> i \<in> {..deg R p}. ?s i) k =
          coeff P (\<Oplus>\<^bsub>P\<^esub> i \<in> {..k} \<union> {k<..deg R p}. ?s i) k"
      by (simp only: ivl_disj_un_one)
    also from True
    have "... = coeff P (\<Oplus>\<^bsub>P\<^esub> i \<in> {..k}. ?s i) k"
      by (simp cong: R.finsum_cong add: R.finsum_Un_disjoint
        ivl_disj_int_one order_less_imp_not_eq2 coeff_finsum R RR Pi_def)
    also
    have "... = coeff P (\<Oplus>\<^bsub>P\<^esub> i \<in> {..<k} \<union> {k}. ?s i) k"
      by (simp only: ivl_disj_un_singleton)
    also have "... = coeff P p k"
      by (simp cong: R.finsum_cong
	add: ivl_disj_int_singleton coeff_finsum deg_aboveD R RR Pi_def)
    finally show ?thesis .
  next
    case False
    hence "coeff P (\<Oplus>\<^bsub>P\<^esub> i \<in> {..deg R p}. ?s i) k =
          coeff P (\<Oplus>\<^bsub>P\<^esub> i \<in> {..<deg R p} \<union> {deg R p}. ?s i) k"
      by (simp only: ivl_disj_un_singleton)
    also from False have "... = coeff P p k"
      by (simp cong: R.finsum_cong
	add: ivl_disj_int_singleton coeff_finsum deg_aboveD R Pi_def)
    finally show ?thesis .
  qed
qed (simp_all add: R Pi_def)

lemma (in UP_cring) up_repr_le:
  "[| deg R p <= n; p \<in> carrier P |] ==>
  (\<Oplus>\<^bsub>P\<^esub> i \<in> {..n}. monom P (coeff P p i) i) = p"
proof -
  let ?s = "(%i. monom P (coeff P p i) i)"
  assume R: "p \<in> carrier P" and "deg R p <= n"
  then have "finsum P ?s {..n} = finsum P ?s ({..deg R p} \<union> {deg R p<..n})"
    by (simp only: ivl_disj_un_one)
  also have "... = finsum P ?s {..deg R p}"
    by (simp cong: P.finsum_cong add: P.finsum_Un_disjoint ivl_disj_int_one
      deg_aboveD R Pi_def)
  also have "... = p" using R by (rule up_repr)
  finally show ?thesis .
qed


subsection {* Polynomials over Integral Domains *}

lemma domainI:
  assumes cring: "cring R"
    and one_not_zero: "one R ~= zero R"
    and integral: "!!a b. [| mult R a b = zero R; a \<in> carrier R;
      b \<in> carrier R |] ==> a = zero R | b = zero R"
  shows "domain R"
  by (auto intro!: domain.intro domain_axioms.intro cring.axioms prems
    del: disjCI)

lemma (in UP_domain) UP_one_not_zero:
  "\<one>\<^bsub>P\<^esub> ~= \<zero>\<^bsub>P\<^esub>"
proof
  assume "\<one>\<^bsub>P\<^esub> = \<zero>\<^bsub>P\<^esub>"
  hence "coeff P \<one>\<^bsub>P\<^esub> 0 = (coeff P \<zero>\<^bsub>P\<^esub> 0)" by simp
  hence "\<one> = \<zero>" by simp
  with one_not_zero show "False" by contradiction
qed

lemma (in UP_domain) UP_integral:
  "[| p \<otimes>\<^bsub>P\<^esub> q = \<zero>\<^bsub>P\<^esub>; p \<in> carrier P; q \<in> carrier P |] ==> p = \<zero>\<^bsub>P\<^esub> | q = \<zero>\<^bsub>P\<^esub>"
proof -
  fix p q
  assume pq: "p \<otimes>\<^bsub>P\<^esub> q = \<zero>\<^bsub>P\<^esub>" and R: "p \<in> carrier P" "q \<in> carrier P"
  show "p = \<zero>\<^bsub>P\<^esub> | q = \<zero>\<^bsub>P\<^esub>"
  proof (rule classical)
    assume c: "~ (p = \<zero>\<^bsub>P\<^esub> | q = \<zero>\<^bsub>P\<^esub>)"
    with R have "deg R p + deg R q = deg R (p \<otimes>\<^bsub>P\<^esub> q)" by simp
    also from pq have "... = 0" by simp
    finally have "deg R p + deg R q = 0" .
    then have f1: "deg R p = 0 & deg R q = 0" by simp
    from f1 R have "p = (\<Oplus>\<^bsub>P\<^esub> i \<in> {..0}. monom P (coeff P p i) i)"
      by (simp only: up_repr_le)
    also from R have "... = monom P (coeff P p 0) 0" by simp
    finally have p: "p = monom P (coeff P p 0) 0" .
    from f1 R have "q = (\<Oplus>\<^bsub>P\<^esub> i \<in> {..0}. monom P (coeff P q i) i)"
      by (simp only: up_repr_le)
    also from R have "... = monom P (coeff P q 0) 0" by simp
    finally have q: "q = monom P (coeff P q 0) 0" .
    from R have "coeff P p 0 \<otimes> coeff P q 0 = coeff P (p \<otimes>\<^bsub>P\<^esub> q) 0" by simp
    also from pq have "... = \<zero>" by simp
    finally have "coeff P p 0 \<otimes> coeff P q 0 = \<zero>" .
    with R have "coeff P p 0 = \<zero> | coeff P q 0 = \<zero>"
      by (simp add: R.integral_iff)
    with p q show "p = \<zero>\<^bsub>P\<^esub> | q = \<zero>\<^bsub>P\<^esub>" by fastsimp
  qed
qed

theorem (in UP_domain) UP_domain:
  "domain P"
  by (auto intro!: domainI UP_cring UP_one_not_zero UP_integral del: disjCI)

text {*
  Interpretation of theorems from @{term domain}.
*}

interpretation UP_domain < "domain" P
  by intro_locales (rule domain.axioms UP_domain)+


subsection {* The Evaluation Homomorphism and Universal Property*}

(* alternative congruence rule (possibly more efficient)
lemma (in abelian_monoid) finsum_cong2:
  "[| !!i. i \<in> A ==> f i \<in> carrier G = True; A = B;
  !!i. i \<in> B ==> f i = g i |] ==> finsum G f A = finsum G g B"
  sorry*)

theorem (in cring) diagonal_sum:
  "[| f \<in> {..n + m::nat} -> carrier R; g \<in> {..n + m} -> carrier R |] ==>
  (\<Oplus>k \<in> {..n + m}. \<Oplus>i \<in> {..k}. f i \<otimes> g (k - i)) =
  (\<Oplus>k \<in> {..n + m}. \<Oplus>i \<in> {..n + m - k}. f k \<otimes> g i)"
proof -
  assume Rf: "f \<in> {..n + m} -> carrier R" and Rg: "g \<in> {..n + m} -> carrier R"
  {
    fix j
    have "j <= n + m ==>
      (\<Oplus>k \<in> {..j}. \<Oplus>i \<in> {..k}. f i \<otimes> g (k - i)) =
      (\<Oplus>k \<in> {..j}. \<Oplus>i \<in> {..j - k}. f k \<otimes> g i)"
    proof (induct j)
      case 0 from Rf Rg show ?case by (simp add: Pi_def)
    next
      case (Suc j)
      have R6: "!!i k. [| k <= j; i <= Suc j - k |] ==> g i \<in> carrier R"
        using Suc by (auto intro!: funcset_mem [OF Rg])
      have R8: "!!i k. [| k <= Suc j; i <= k |] ==> g (k - i) \<in> carrier R"
        using Suc by (auto intro!: funcset_mem [OF Rg])
      have R9: "!!i k. [| k <= Suc j |] ==> f k \<in> carrier R"
        using Suc by (auto intro!: funcset_mem [OF Rf])
      have R10: "!!i k. [| k <= Suc j; i <= Suc j - k |] ==> g i \<in> carrier R"
        using Suc by (auto intro!: funcset_mem [OF Rg])
      have R11: "g 0 \<in> carrier R"
        using Suc by (auto intro!: funcset_mem [OF Rg])
      from Suc show ?case
        by (simp cong: finsum_cong add: Suc_diff_le a_ac
          Pi_def R6 R8 R9 R10 R11)
    qed
  }
  then show ?thesis by fast
qed

lemma (in abelian_monoid) boundD_carrier:
  "[| bound \<zero> n f; n < m |] ==> f m \<in> carrier G"
  by auto

theorem (in cring) cauchy_product:
  assumes bf: "bound \<zero> n f" and bg: "bound \<zero> m g"
    and Rf: "f \<in> {..n} -> carrier R" and Rg: "g \<in> {..m} -> carrier R"
  shows "(\<Oplus>k \<in> {..n + m}. \<Oplus>i \<in> {..k}. f i \<otimes> g (k - i)) =
    (\<Oplus>i \<in> {..n}. f i) \<otimes> (\<Oplus>i \<in> {..m}. g i)"      (* State reverse direction? *)
proof -
  have f: "!!x. f x \<in> carrier R"
  proof -
    fix x
    show "f x \<in> carrier R"
      using Rf bf boundD_carrier by (cases "x <= n") (auto simp: Pi_def)
  qed
  have g: "!!x. g x \<in> carrier R"
  proof -
    fix x
    show "g x \<in> carrier R"
      using Rg bg boundD_carrier by (cases "x <= m") (auto simp: Pi_def)
  qed
  from f g have "(\<Oplus>k \<in> {..n + m}. \<Oplus>i \<in> {..k}. f i \<otimes> g (k - i)) =
      (\<Oplus>k \<in> {..n + m}. \<Oplus>i \<in> {..n + m - k}. f k \<otimes> g i)"
    by (simp add: diagonal_sum Pi_def)
  also have "... = (\<Oplus>k \<in> {..n} \<union> {n<..n + m}. \<Oplus>i \<in> {..n + m - k}. f k \<otimes> g i)"
    by (simp only: ivl_disj_un_one)
  also from f g have "... = (\<Oplus>k \<in> {..n}. \<Oplus>i \<in> {..n + m - k}. f k \<otimes> g i)"
    by (simp cong: finsum_cong
      add: bound.bound [OF bf] finsum_Un_disjoint ivl_disj_int_one Pi_def)
  also from f g
  have "... = (\<Oplus>k \<in> {..n}. \<Oplus>i \<in> {..m} \<union> {m<..n + m - k}. f k \<otimes> g i)"
    by (simp cong: finsum_cong add: ivl_disj_un_one le_add_diff Pi_def)
  also from f g have "... = (\<Oplus>k \<in> {..n}. \<Oplus>i \<in> {..m}. f k \<otimes> g i)"
    by (simp cong: finsum_cong
      add: bound.bound [OF bg] finsum_Un_disjoint ivl_disj_int_one Pi_def)
  also from f g have "... = (\<Oplus>i \<in> {..n}. f i) \<otimes> (\<Oplus>i \<in> {..m}. g i)"
    by (simp add: finsum_ldistr diagonal_sum Pi_def,
      simp cong: finsum_cong add: finsum_rdistr Pi_def)
  finally show ?thesis .
qed

lemma (in UP_cring) const_ring_hom:
  "(%a. monom P a 0) \<in> ring_hom R P"
  by (auto intro!: ring_hom_memI intro: up_eqI simp: monom_mult_is_smult)

constdefs (structure S)
  eval :: "[('a, 'm) ring_scheme, ('b, 'n) ring_scheme,
           'a => 'b, 'b, nat => 'a] => 'b"
  "eval R S phi s == \<lambda>p \<in> carrier (UP R).
    \<Oplus>i \<in> {..deg R p}. phi (coeff (UP R) p i) \<otimes> s (^) i"


lemma (in UP) eval_on_carrier:
  fixes S (structure)
  shows "p \<in> carrier P ==>
  eval R S phi s p = (\<Oplus>\<^bsub>S\<^esub> i \<in> {..deg R p}. phi (coeff P p i) \<otimes>\<^bsub>S\<^esub> s (^)\<^bsub>S\<^esub> i)"
  by (unfold eval_def, fold P_def) simp

lemma (in UP) eval_extensional:
  "eval R S phi p \<in> extensional (carrier P)"
  by (unfold eval_def, fold P_def) simp


text {* The universal property of the polynomial ring *}

locale UP_pre_univ_prop = ring_hom_cring R S h + UP_cring R P

locale UP_univ_prop = UP_pre_univ_prop +
  fixes s and Eval
  assumes indet_img_carrier [simp, intro]: "s \<in> carrier S"
  defines Eval_def: "Eval == eval R S h s"

theorem (in UP_pre_univ_prop) eval_ring_hom:
  assumes S: "s \<in> carrier S"
  shows "eval R S h s \<in> ring_hom P S"
proof (rule ring_hom_memI)
  fix p
  assume R: "p \<in> carrier P"
  then show "eval R S h s p \<in> carrier S"
    by (simp only: eval_on_carrier) (simp add: S Pi_def)
next
  fix p q
  assume R: "p \<in> carrier P" "q \<in> carrier P"
  then show "eval R S h s (p \<otimes>\<^bsub>P\<^esub> q) = eval R S h s p \<otimes>\<^bsub>S\<^esub> eval R S h s q"
  proof (simp only: eval_on_carrier UP_mult_closed)
    from R S have
      "(\<Oplus>\<^bsub>S\<^esub> i \<in> {..deg R (p \<otimes>\<^bsub>P\<^esub> q)}. h (coeff P (p \<otimes>\<^bsub>P\<^esub> q) i) \<otimes>\<^bsub>S\<^esub> s (^)\<^bsub>S\<^esub> i) =
      (\<Oplus>\<^bsub>S\<^esub> i \<in> {..deg R (p \<otimes>\<^bsub>P\<^esub> q)} \<union> {deg R (p \<otimes>\<^bsub>P\<^esub> q)<..deg R p + deg R q}.
        h (coeff P (p \<otimes>\<^bsub>P\<^esub> q) i) \<otimes>\<^bsub>S\<^esub> s (^)\<^bsub>S\<^esub> i)"
      by (simp cong: S.finsum_cong
        add: deg_aboveD S.finsum_Un_disjoint ivl_disj_int_one Pi_def
        del: coeff_mult)
    also from R have "... =
      (\<Oplus>\<^bsub>S\<^esub> i \<in> {..deg R p + deg R q}. h (coeff P (p \<otimes>\<^bsub>P\<^esub> q) i) \<otimes>\<^bsub>S\<^esub> s (^)\<^bsub>S\<^esub> i)"
      by (simp only: ivl_disj_un_one deg_mult_cring)
    also from R S have "... =
      (\<Oplus>\<^bsub>S\<^esub> i \<in> {..deg R p + deg R q}.
         \<Oplus>\<^bsub>S\<^esub> k \<in> {..i}.
           h (coeff P p k) \<otimes>\<^bsub>S\<^esub> h (coeff P q (i - k)) \<otimes>\<^bsub>S\<^esub>
           (s (^)\<^bsub>S\<^esub> k \<otimes>\<^bsub>S\<^esub> s (^)\<^bsub>S\<^esub> (i - k)))"
      by (simp cong: S.finsum_cong add: S.nat_pow_mult Pi_def
        S.m_ac S.finsum_rdistr)
    also from R S have "... =
      (\<Oplus>\<^bsub>S\<^esub> i\<in>{..deg R p}. h (coeff P p i) \<otimes>\<^bsub>S\<^esub> s (^)\<^bsub>S\<^esub> i) \<otimes>\<^bsub>S\<^esub>
      (\<Oplus>\<^bsub>S\<^esub> i\<in>{..deg R q}. h (coeff P q i) \<otimes>\<^bsub>S\<^esub> s (^)\<^bsub>S\<^esub> i)"
      by (simp add: S.cauchy_product [THEN sym] bound.intro deg_aboveD S.m_ac
        Pi_def)
    finally show
      "(\<Oplus>\<^bsub>S\<^esub> i \<in> {..deg R (p \<otimes>\<^bsub>P\<^esub> q)}. h (coeff P (p \<otimes>\<^bsub>P\<^esub> q) i) \<otimes>\<^bsub>S\<^esub> s (^)\<^bsub>S\<^esub> i) =
      (\<Oplus>\<^bsub>S\<^esub> i \<in> {..deg R p}. h (coeff P p i) \<otimes>\<^bsub>S\<^esub> s (^)\<^bsub>S\<^esub> i) \<otimes>\<^bsub>S\<^esub>
      (\<Oplus>\<^bsub>S\<^esub> i \<in> {..deg R q}. h (coeff P q i) \<otimes>\<^bsub>S\<^esub> s (^)\<^bsub>S\<^esub> i)" .
  qed
next
  fix p q
  assume R: "p \<in> carrier P" "q \<in> carrier P"
  then show "eval R S h s (p \<oplus>\<^bsub>P\<^esub> q) = eval R S h s p \<oplus>\<^bsub>S\<^esub> eval R S h s q"
  proof (simp only: eval_on_carrier P.a_closed)
    from S R have
      "(\<Oplus>\<^bsub>S \<^esub>i\<in>{..deg R (p \<oplus>\<^bsub>P\<^esub> q)}. h (coeff P (p \<oplus>\<^bsub>P\<^esub> q) i) \<otimes>\<^bsub>S\<^esub> s (^)\<^bsub>S\<^esub> i) =
      (\<Oplus>\<^bsub>S \<^esub>i\<in>{..deg R (p \<oplus>\<^bsub>P\<^esub> q)} \<union> {deg R (p \<oplus>\<^bsub>P\<^esub> q)<..max (deg R p) (deg R q)}.
        h (coeff P (p \<oplus>\<^bsub>P\<^esub> q) i) \<otimes>\<^bsub>S\<^esub> s (^)\<^bsub>S\<^esub> i)"
      by (simp cong: S.finsum_cong
        add: deg_aboveD S.finsum_Un_disjoint ivl_disj_int_one Pi_def
        del: coeff_add)
    also from R have "... =
        (\<Oplus>\<^bsub>S\<^esub> i \<in> {..max (deg R p) (deg R q)}.
          h (coeff P (p \<oplus>\<^bsub>P\<^esub> q) i) \<otimes>\<^bsub>S\<^esub> s (^)\<^bsub>S\<^esub> i)"
      by (simp add: ivl_disj_un_one)
    also from R S have "... =
      (\<Oplus>\<^bsub>S\<^esub>i\<in>{..max (deg R p) (deg R q)}. h (coeff P p i) \<otimes>\<^bsub>S\<^esub> s (^)\<^bsub>S\<^esub> i) \<oplus>\<^bsub>S\<^esub>
      (\<Oplus>\<^bsub>S\<^esub>i\<in>{..max (deg R p) (deg R q)}. h (coeff P q i) \<otimes>\<^bsub>S\<^esub> s (^)\<^bsub>S\<^esub> i)"
      by (simp cong: S.finsum_cong
        add: S.l_distr deg_aboveD ivl_disj_int_one Pi_def)
    also have "... =
        (\<Oplus>\<^bsub>S\<^esub> i \<in> {..deg R p} \<union> {deg R p<..max (deg R p) (deg R q)}.
          h (coeff P p i) \<otimes>\<^bsub>S\<^esub> s (^)\<^bsub>S\<^esub> i) \<oplus>\<^bsub>S\<^esub>
        (\<Oplus>\<^bsub>S\<^esub> i \<in> {..deg R q} \<union> {deg R q<..max (deg R p) (deg R q)}.
          h (coeff P q i) \<otimes>\<^bsub>S\<^esub> s (^)\<^bsub>S\<^esub> i)"
      by (simp only: ivl_disj_un_one le_maxI1 le_maxI2)
    also from R S have "... =
      (\<Oplus>\<^bsub>S\<^esub> i \<in> {..deg R p}. h (coeff P p i) \<otimes>\<^bsub>S\<^esub> s (^)\<^bsub>S\<^esub> i) \<oplus>\<^bsub>S\<^esub>
      (\<Oplus>\<^bsub>S\<^esub> i \<in> {..deg R q}. h (coeff P q i) \<otimes>\<^bsub>S\<^esub> s (^)\<^bsub>S\<^esub> i)"
      by (simp cong: S.finsum_cong
        add: deg_aboveD S.finsum_Un_disjoint ivl_disj_int_one Pi_def)
    finally show
      "(\<Oplus>\<^bsub>S\<^esub>i \<in> {..deg R (p \<oplus>\<^bsub>P\<^esub> q)}. h (coeff P (p \<oplus>\<^bsub>P\<^esub> q) i) \<otimes>\<^bsub>S\<^esub> s (^)\<^bsub>S\<^esub> i) =
      (\<Oplus>\<^bsub>S\<^esub>i \<in> {..deg R p}. h (coeff P p i) \<otimes>\<^bsub>S\<^esub> s (^)\<^bsub>S\<^esub> i) \<oplus>\<^bsub>S\<^esub>
      (\<Oplus>\<^bsub>S\<^esub>i \<in> {..deg R q}. h (coeff P q i) \<otimes>\<^bsub>S\<^esub> s (^)\<^bsub>S\<^esub> i)" .
  qed
next
  show "eval R S h s \<one>\<^bsub>P\<^esub> = \<one>\<^bsub>S\<^esub>"
    by (simp only: eval_on_carrier UP_one_closed) simp
qed

text {* Interpretation of ring homomorphism lemmas. *}

interpretation UP_univ_prop < ring_hom_cring P S Eval
  apply (unfold Eval_def)
  apply intro_locales
  apply (rule ring_hom_cring.axioms)
  apply (rule ring_hom_cring.intro)
  apply unfold_locales
  apply (rule eval_ring_hom)
  apply rule
  done


text {* Further properties of the evaluation homomorphism. *}

text {*
  The following lemma could be proved in @{text UP_cring} with the additional
  assumption that @{text h} is closed. *}

lemma (in UP_pre_univ_prop) eval_const:
  "[| s \<in> carrier S; r \<in> carrier R |] ==> eval R S h s (monom P r 0) = h r"
  by (simp only: eval_on_carrier monom_closed) simp

text {* The following proof is complicated by the fact that in arbitrary
  rings one might have @{term "one R = zero R"}. *}

(* TODO: simplify by cases "one R = zero R" *)

lemma (in UP_pre_univ_prop) eval_monom1:
  assumes S: "s \<in> carrier S"
  shows "eval R S h s (monom P \<one> 1) = s"
proof (simp only: eval_on_carrier monom_closed R.one_closed)
   from S have
    "(\<Oplus>\<^bsub>S\<^esub> i\<in>{..deg R (monom P \<one> 1)}. h (coeff P (monom P \<one> 1) i) \<otimes>\<^bsub>S\<^esub> s (^)\<^bsub>S\<^esub> i) =
    (\<Oplus>\<^bsub>S\<^esub> i\<in>{..deg R (monom P \<one> 1)} \<union> {deg R (monom P \<one> 1)<..1}.
      h (coeff P (monom P \<one> 1) i) \<otimes>\<^bsub>S\<^esub> s (^)\<^bsub>S\<^esub> i)"
    by (simp cong: S.finsum_cong del: coeff_monom
      add: deg_aboveD S.finsum_Un_disjoint ivl_disj_int_one Pi_def)
  also have "... =
    (\<Oplus>\<^bsub>S\<^esub> i \<in> {..1}. h (coeff P (monom P \<one> 1) i) \<otimes>\<^bsub>S\<^esub> s (^)\<^bsub>S\<^esub> i)"
    by (simp only: ivl_disj_un_one deg_monom_le R.one_closed)
  also have "... = s"
  proof (cases "s = \<zero>\<^bsub>S\<^esub>")
    case True then show ?thesis by (simp add: Pi_def)
  next
    case False then show ?thesis by (simp add: S Pi_def)
  qed
  finally show "(\<Oplus>\<^bsub>S\<^esub> i \<in> {..deg R (monom P \<one> 1)}.
    h (coeff P (monom P \<one> 1) i) \<otimes>\<^bsub>S\<^esub> s (^)\<^bsub>S\<^esub> i) = s" .
qed

lemma (in UP_cring) monom_pow:
  assumes R: "a \<in> carrier R"
  shows "(monom P a n) (^)\<^bsub>P\<^esub> m = monom P (a (^) m) (n * m)"
proof (induct m)
  case 0 from R show ?case by simp
next
  case Suc with R show ?case
    by (simp del: monom_mult add: monom_mult [THEN sym] add_commute)
qed

lemma (in ring_hom_cring) hom_pow [simp]:
  "x \<in> carrier R ==> h (x (^) n) = h x (^)\<^bsub>S\<^esub> (n::nat)"
  by (induct n) simp_all

lemma (in UP_univ_prop) Eval_monom:
  "r \<in> carrier R ==> Eval (monom P r n) = h r \<otimes>\<^bsub>S\<^esub> s (^)\<^bsub>S\<^esub> n"
proof -
  assume R: "r \<in> carrier R"
  from R have "Eval (monom P r n) = Eval (monom P r 0 \<otimes>\<^bsub>P\<^esub> (monom P \<one> 1) (^)\<^bsub>P\<^esub> n)"
    by (simp del: monom_mult add: monom_mult [THEN sym] monom_pow)
  also
  from R eval_monom1 [where s = s, folded Eval_def]
  have "... = h r \<otimes>\<^bsub>S\<^esub> s (^)\<^bsub>S\<^esub> n"
    by (simp add: eval_const [where s = s, folded Eval_def])
  finally show ?thesis .
qed

lemma (in UP_pre_univ_prop) eval_monom:
  assumes R: "r \<in> carrier R" and S: "s \<in> carrier S"
  shows "eval R S h s (monom P r n) = h r \<otimes>\<^bsub>S\<^esub> s (^)\<^bsub>S\<^esub> n"
proof -
  interpret UP_univ_prop [R S h P s _]
    using UP_pre_univ_prop_axioms P_def R S
    by (auto intro: UP_univ_prop.intro UP_univ_prop_axioms.intro)
  from R
  show ?thesis by (rule Eval_monom)
qed

lemma (in UP_univ_prop) Eval_smult:
  "[| r \<in> carrier R; p \<in> carrier P |] ==> Eval (r \<odot>\<^bsub>P\<^esub> p) = h r \<otimes>\<^bsub>S\<^esub> Eval p"
proof -
  assume R: "r \<in> carrier R" and P: "p \<in> carrier P"
  then show ?thesis
    by (simp add: monom_mult_is_smult [THEN sym]
      eval_const [where s = s, folded Eval_def])
qed

lemma ring_hom_cringI:
  assumes "cring R"
    and "cring S"
    and "h \<in> ring_hom R S"
  shows "ring_hom_cring R S h"
  by (fast intro: ring_hom_cring.intro ring_hom_cring_axioms.intro
    cring.axioms prems)

lemma (in UP_pre_univ_prop) UP_hom_unique:
  includes ring_hom_cring P S Phi
  assumes Phi: "Phi (monom P \<one> (Suc 0)) = s"
      "!!r. r \<in> carrier R ==> Phi (monom P r 0) = h r"
  includes ring_hom_cring P S Psi
  assumes Psi: "Psi (monom P \<one> (Suc 0)) = s"
      "!!r. r \<in> carrier R ==> Psi (monom P r 0) = h r"
    and P: "p \<in> carrier P" and S: "s \<in> carrier S"
  shows "Phi p = Psi p"
proof -
  have "Phi p =
      Phi (\<Oplus>\<^bsub>P \<^esub>i \<in> {..deg R p}. monom P (coeff P p i) 0 \<otimes>\<^bsub>P\<^esub> monom P \<one> 1 (^)\<^bsub>P\<^esub> i)"
    by (simp add: up_repr P monom_mult [THEN sym] monom_pow del: monom_mult)
  also
  have "... =
      Psi (\<Oplus>\<^bsub>P \<^esub>i\<in>{..deg R p}. monom P (coeff P p i) 0 \<otimes>\<^bsub>P\<^esub> monom P \<one> 1 (^)\<^bsub>P\<^esub> i)"
    by (simp add: Phi Psi P Pi_def comp_def)
  also have "... = Psi p"
    by (simp add: up_repr P monom_mult [THEN sym] monom_pow del: monom_mult)
  finally show ?thesis .
qed

lemma (in UP_pre_univ_prop) ring_homD:
  assumes Phi: "Phi \<in> ring_hom P S"
  shows "ring_hom_cring P S Phi"
proof (rule ring_hom_cring.intro)
  show "ring_hom_cring_axioms P S Phi"
  by (rule ring_hom_cring_axioms.intro) (rule Phi)
qed unfold_locales

theorem (in UP_pre_univ_prop) UP_universal_property:
  assumes S: "s \<in> carrier S"
  shows "EX! Phi. Phi \<in> ring_hom P S \<inter> extensional (carrier P) &
    Phi (monom P \<one> 1) = s &
    (ALL r : carrier R. Phi (monom P r 0) = h r)"
  using S eval_monom1
  apply (auto intro: eval_ring_hom eval_const eval_extensional)
  apply (rule extensionalityI)
  apply (auto intro: UP_hom_unique ring_homD)
  done


subsection {* Sample Application of Evaluation Homomorphism *}

lemma UP_pre_univ_propI:
  assumes "cring R"
    and "cring S"
    and "h \<in> ring_hom R S"
  shows "UP_pre_univ_prop R S h"
  using assms
  by (auto intro!: UP_pre_univ_prop.intro ring_hom_cring.intro
    ring_hom_cring_axioms.intro UP_cring.intro)

constdefs
  INTEG :: "int ring"
  "INTEG == (| carrier = UNIV, mult = op *, one = 1, zero = 0, add = op + |)"

lemma INTEG_cring:
  "cring INTEG"
  by (unfold INTEG_def) (auto intro!: cringI abelian_groupI comm_monoidI
    zadd_zminus_inverse2 zadd_zmult_distrib)

lemma INTEG_id_eval:
  "UP_pre_univ_prop INTEG INTEG id"
  by (fast intro: UP_pre_univ_propI INTEG_cring id_ring_hom)

text {*
  Interpretation now enables to import all theorems and lemmas
  valid in the context of homomorphisms between @{term INTEG} and @{term
  "UP INTEG"} globally.
*}

interpretation INTEG: UP_pre_univ_prop [INTEG INTEG id]
  apply simp
  using INTEG_id_eval
  apply simp
  done

lemma INTEG_closed [intro, simp]:
  "z \<in> carrier INTEG"
  by (unfold INTEG_def) simp

lemma INTEG_mult [simp]:
  "mult INTEG z w = z * w"
  by (unfold INTEG_def) simp

lemma INTEG_pow [simp]:
  "pow INTEG z n = z ^ n"
  by (induct n) (simp_all add: INTEG_def nat_pow_def)

lemma "eval INTEG INTEG id 10 (monom (UP INTEG) 5 2) = 500"
  by (simp add: INTEG.eval_monom)

end
