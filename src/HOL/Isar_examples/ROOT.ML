(*  Title:      HOL/Isar_examples/ROOT.ML
    ID:         $Id: ROOT.ML,v 1.26 2007/08/10 20:31:19 wenzelm Exp $
    Author:     Markus Wenzel, TU Muenchen

Miscellaneous Isabelle/Isar examples for Higher-Order Logic.
*)

no_document use_thys ["../NumberTheory/Primes", "../NumberTheory/Fibonacci"];

use_thys [
  "BasicLogic",
  "Cantor",
  "Peirce",
  "Drinker",
  "ExprCompiler",
  "Group",
  "Summation",
  "KnasterTarski",
  "MutilatedCheckerboard",
  "Puzzle",
  "NestedDatatype",
  "HoareEx"
];
