(*  Title:      HOL/ZF/MainZF.thy
    ID:         $Id: MainZF.thy,v 1.1 2006/03/07 15:03:31 obua Exp $
    Author:     Steven Obua

    Starting point for using HOLZF.
    See "Partizan Games in Isabelle/HOLZF", available from http://www4.in.tum.de/~obua/partizan
*)

theory MainZF
imports Zet LProd
begin
end