(* $Id: Lambda_mu.thy,v 1.6 2008/01/08 22:11:08 urbanc Exp $ *)

theory Lambda_mu 
  imports "../Nominal" 
begin

section {* Lambda-Mu according to a paper by Gavin Bierman *}

atom_decl var mvar

nominal_datatype trm = 
    Var   "var" 
  | Lam  "\<guillemotleft>var\<guillemotright>trm"   ("Lam [_]._" [100,100] 100)
  | App  "trm" "trm" 
  | Pss  "mvar" "trm"                                   (* passivate *)
  | Act  "\<guillemotleft>mvar\<guillemotright>trm"  ("Act [_]._" [100,100] 100)       (* activate  *)


end
