(*
    File:        RPCMemoryParams.thy
    ID:          $Id: RPCMemoryParams.thy,v 1.6 2006/12/02 01:52:05 wenzelm Exp $
    Author:      Stephan Merz
    Copyright:   1997 University of Munich
*)

header {* Basic declarations for the RPC-memory example *}

theory RPCMemoryParams
imports Main
begin

types
  bit = "bool"   (* Signal wires for the procedure interface.
                    Defined as bool for simplicity. All I should really need is
                    the existence of two distinct values. *)

(* all of these are simple (HOL) types *)
typedecl Locs    (* "syntactic" value type *)
typedecl Vals    (* "syntactic" value type *)
typedecl PrIds   (* process id's *)

end
