(*  Title:      HOL/Complex/Complex_Main.thy
    ID:         $Id: Complex_Main.thy,v 1.7 2008/04/24 14:53:10 haftmann Exp $
    Author:     Lawrence C Paulson, Cambridge University Computer Laboratory
    Copyright   2003  University of Cambridge
*)

header{*Comprehensive Complex Theory*}

theory Complex_Main
imports Fundamental_Theorem_Algebra CLim "../Hyperreal/Hyperreal"
begin

end
