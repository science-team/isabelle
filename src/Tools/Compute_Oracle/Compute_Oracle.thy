(*  Title:      Tools/Compute_Oracle/Compute_Oracle.thy
    ID:         $Id: Compute_Oracle.thy,v 1.4 2008/05/18 15:03:22 wenzelm Exp $
    Author:     Steven Obua, TU Munich

Steven Obua's evaluator.
*)

theory Compute_Oracle imports Pure
uses "am.ML" "am_compiler.ML" "am_interpreter.ML" "am_ghc.ML" "am_sml.ML" "report.ML" "compute.ML" "linker.ML"
begin

setup {* Compute.setup_compute *}

end