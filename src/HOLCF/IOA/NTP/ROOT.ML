(*  Title:      HOLCF/IOA/NTP/ROOT.ML
    ID:         $Id: ROOT.ML,v 1.9 2007/09/15 17:27:38 haftmann Exp $
    Author:     Tobias Nipkow & Konrad Slind

This is the ROOT file for a network transmission protocol (NTP
subdirectory), performed in the I/O automata formalization by Olaf
Mueller.
*)

use_thy "Correctness";
