(*  Title:      HOLCF/IOA/ABP/Packet.thy
    ID:         $Id: Packet.thy,v 1.8 2007/10/21 12:21:51 wenzelm Exp $
    Author:     Olaf M�ller
*)

header {* Packets *}

theory Packet
imports Main
begin

types
  'msg packet = "bool * 'msg"

definition
  hdr :: "'msg packet => bool" where
  "hdr = fst"

definition
  msg :: "'msg packet => 'msg" where
  "msg = snd"

end
