(*  Title:      HOLCF/IOA/ABP/Action.thy
    ID:         $Id: Action.thy,v 1.6 2006/05/27 17:49:36 wenzelm Exp $
    Author:     Olaf M�ller
*)

header {* The set of all actions of the system *}

theory Action
imports Packet
begin

datatype 'm action =
    Next | S_msg 'm | R_msg 'm
  | S_pkt "'m packet" | R_pkt "'m packet"
  | S_ack bool | R_ack bool

end
