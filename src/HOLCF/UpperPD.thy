(*  Title:      HOLCF/UpperPD.thy
    ID:         $Id: UpperPD.thy,v 1.8 2008/05/19 21:49:23 huffman Exp $
    Author:     Brian Huffman
*)

header {* Upper powerdomain *}

theory UpperPD
imports CompactBasis
begin

subsection {* Basis preorder *}

definition
  upper_le :: "'a pd_basis \<Rightarrow> 'a pd_basis \<Rightarrow> bool" (infix "\<le>\<sharp>" 50) where
  "upper_le = (\<lambda>u v. \<forall>y\<in>Rep_pd_basis v. \<exists>x\<in>Rep_pd_basis u. x \<sqsubseteq> y)"

lemma upper_le_refl [simp]: "t \<le>\<sharp> t"
unfolding upper_le_def by fast

lemma upper_le_trans: "\<lbrakk>t \<le>\<sharp> u; u \<le>\<sharp> v\<rbrakk> \<Longrightarrow> t \<le>\<sharp> v"
unfolding upper_le_def
apply (rule ballI)
apply (drule (1) bspec, erule bexE)
apply (drule (1) bspec, erule bexE)
apply (erule rev_bexI)
apply (erule (1) trans_less)
done

interpretation upper_le: preorder [upper_le]
by (rule preorder.intro, rule upper_le_refl, rule upper_le_trans)

lemma upper_le_minimal [simp]: "PDUnit compact_bot \<le>\<sharp> t"
unfolding upper_le_def Rep_PDUnit by simp

lemma PDUnit_upper_mono: "x \<sqsubseteq> y \<Longrightarrow> PDUnit x \<le>\<sharp> PDUnit y"
unfolding upper_le_def Rep_PDUnit by simp

lemma PDPlus_upper_mono: "\<lbrakk>s \<le>\<sharp> t; u \<le>\<sharp> v\<rbrakk> \<Longrightarrow> PDPlus s u \<le>\<sharp> PDPlus t v"
unfolding upper_le_def Rep_PDPlus by fast

lemma PDPlus_upper_less: "PDPlus t u \<le>\<sharp> t"
unfolding upper_le_def Rep_PDPlus by fast

lemma upper_le_PDUnit_PDUnit_iff [simp]:
  "(PDUnit a \<le>\<sharp> PDUnit b) = a \<sqsubseteq> b"
unfolding upper_le_def Rep_PDUnit by fast

lemma upper_le_PDPlus_PDUnit_iff:
  "(PDPlus t u \<le>\<sharp> PDUnit a) = (t \<le>\<sharp> PDUnit a \<or> u \<le>\<sharp> PDUnit a)"
unfolding upper_le_def Rep_PDPlus Rep_PDUnit by fast

lemma upper_le_PDPlus_iff: "(t \<le>\<sharp> PDPlus u v) = (t \<le>\<sharp> u \<and> t \<le>\<sharp> v)"
unfolding upper_le_def Rep_PDPlus by fast

lemma upper_le_induct [induct set: upper_le]:
  assumes le: "t \<le>\<sharp> u"
  assumes 1: "\<And>a b. a \<sqsubseteq> b \<Longrightarrow> P (PDUnit a) (PDUnit b)"
  assumes 2: "\<And>t u a. P t (PDUnit a) \<Longrightarrow> P (PDPlus t u) (PDUnit a)"
  assumes 3: "\<And>t u v. \<lbrakk>P t u; P t v\<rbrakk> \<Longrightarrow> P t (PDPlus u v)"
  shows "P t u"
using le apply (induct u arbitrary: t rule: pd_basis_induct)
apply (erule rev_mp)
apply (induct_tac t rule: pd_basis_induct)
apply (simp add: 1)
apply (simp add: upper_le_PDPlus_PDUnit_iff)
apply (simp add: 2)
apply (subst PDPlus_commute)
apply (simp add: 2)
apply (simp add: upper_le_PDPlus_iff 3)
done

lemma approx_pd_upper_mono1:
  "i \<le> j \<Longrightarrow> approx_pd i t \<le>\<sharp> approx_pd j t"
apply (induct t rule: pd_basis_induct)
apply (simp add: compact_approx_mono1)
apply (simp add: PDPlus_upper_mono)
done

lemma approx_pd_upper_le: "approx_pd i t \<le>\<sharp> t"
apply (induct t rule: pd_basis_induct)
apply (simp add: compact_approx_le)
apply (simp add: PDPlus_upper_mono)
done

lemma approx_pd_upper_mono:
  "t \<le>\<sharp> u \<Longrightarrow> approx_pd n t \<le>\<sharp> approx_pd n u"
apply (erule upper_le_induct)
apply (simp add: compact_approx_mono)
apply (simp add: upper_le_PDPlus_PDUnit_iff)
apply (simp add: upper_le_PDPlus_iff)
done


subsection {* Type definition *}

cpodef (open) 'a upper_pd =
  "{S::'a::profinite pd_basis set. upper_le.ideal S}"
apply (simp add: upper_le.adm_ideal)
apply (fast intro: upper_le.ideal_principal)
done

lemma ideal_Rep_upper_pd: "upper_le.ideal (Rep_upper_pd x)"
by (rule Rep_upper_pd [unfolded mem_Collect_eq])

definition
  upper_principal :: "'a pd_basis \<Rightarrow> 'a upper_pd" where
  "upper_principal t = Abs_upper_pd {u. u \<le>\<sharp> t}"

lemma Rep_upper_principal:
  "Rep_upper_pd (upper_principal t) = {u. u \<le>\<sharp> t}"
unfolding upper_principal_def
apply (rule Abs_upper_pd_inverse [unfolded mem_Collect_eq])
apply (rule upper_le.ideal_principal)
done

interpretation upper_pd:
  ideal_completion [upper_le approx_pd upper_principal Rep_upper_pd]
apply unfold_locales
apply (rule approx_pd_upper_le)
apply (rule approx_pd_idem)
apply (erule approx_pd_upper_mono)
apply (rule approx_pd_upper_mono1, simp)
apply (rule finite_range_approx_pd)
apply (rule ex_approx_pd_eq)
apply (rule ideal_Rep_upper_pd)
apply (rule cont_Rep_upper_pd)
apply (rule Rep_upper_principal)
apply (simp only: less_upper_pd_def less_set_eq)
done

lemma upper_principal_less_iff [simp]:
  "upper_principal t \<sqsubseteq> upper_principal u \<longleftrightarrow> t \<le>\<sharp> u"
by (rule upper_pd.principal_less_iff)

lemma upper_principal_eq_iff:
  "upper_principal t = upper_principal u \<longleftrightarrow> t \<le>\<sharp> u \<and> u \<le>\<sharp> t"
by (rule upper_pd.principal_eq_iff)

lemma upper_principal_mono:
  "t \<le>\<sharp> u \<Longrightarrow> upper_principal t \<sqsubseteq> upper_principal u"
by (rule upper_pd.principal_mono)

lemma compact_upper_principal: "compact (upper_principal t)"
by (rule upper_pd.compact_principal)

lemma upper_pd_minimal: "upper_principal (PDUnit compact_bot) \<sqsubseteq> ys"
by (induct ys rule: upper_pd.principal_induct, simp, simp)

instance upper_pd :: (bifinite) pcpo
by intro_classes (fast intro: upper_pd_minimal)

lemma inst_upper_pd_pcpo: "\<bottom> = upper_principal (PDUnit compact_bot)"
by (rule upper_pd_minimal [THEN UU_I, symmetric])


subsection {* Approximation *}

instantiation upper_pd :: (profinite) profinite
begin

definition
  approx_upper_pd_def: "approx = upper_pd.completion_approx"

instance
apply (intro_classes, unfold approx_upper_pd_def)
apply (simp add: upper_pd.chain_completion_approx)
apply (rule upper_pd.lub_completion_approx)
apply (rule upper_pd.completion_approx_idem)
apply (rule upper_pd.finite_fixes_completion_approx)
done

end

instance upper_pd :: (bifinite) bifinite ..

lemma approx_upper_principal [simp]:
  "approx n\<cdot>(upper_principal t) = upper_principal (approx_pd n t)"
unfolding approx_upper_pd_def
by (rule upper_pd.completion_approx_principal)

lemma approx_eq_upper_principal:
  "\<exists>t\<in>Rep_upper_pd xs. approx n\<cdot>xs = upper_principal (approx_pd n t)"
unfolding approx_upper_pd_def
by (rule upper_pd.completion_approx_eq_principal)

lemma compact_imp_upper_principal:
  "compact xs \<Longrightarrow> \<exists>t. xs = upper_principal t"
apply (drule bifinite_compact_eq_approx)
apply (erule exE)
apply (erule subst)
apply (cut_tac n=i and xs=xs in approx_eq_upper_principal)
apply fast
done

lemma upper_principal_induct:
  "\<lbrakk>adm P; \<And>t. P (upper_principal t)\<rbrakk> \<Longrightarrow> P xs"
by (rule upper_pd.principal_induct)

lemma upper_principal_induct2:
  "\<lbrakk>\<And>ys. adm (\<lambda>xs. P xs ys); \<And>xs. adm (\<lambda>ys. P xs ys);
    \<And>t u. P (upper_principal t) (upper_principal u)\<rbrakk> \<Longrightarrow> P xs ys"
apply (rule_tac x=ys in spec)
apply (rule_tac xs=xs in upper_principal_induct, simp)
apply (rule allI, rename_tac ys)
apply (rule_tac xs=ys in upper_principal_induct, simp)
apply simp
done


subsection {* Monadic unit and plus *}

definition
  upper_unit :: "'a \<rightarrow> 'a upper_pd" where
  "upper_unit = compact_basis.basis_fun (\<lambda>a. upper_principal (PDUnit a))"

definition
  upper_plus :: "'a upper_pd \<rightarrow> 'a upper_pd \<rightarrow> 'a upper_pd" where
  "upper_plus = upper_pd.basis_fun (\<lambda>t. upper_pd.basis_fun (\<lambda>u.
      upper_principal (PDPlus t u)))"

abbreviation
  upper_add :: "'a upper_pd \<Rightarrow> 'a upper_pd \<Rightarrow> 'a upper_pd"
    (infixl "+\<sharp>" 65) where
  "xs +\<sharp> ys == upper_plus\<cdot>xs\<cdot>ys"

syntax
  "_upper_pd" :: "args \<Rightarrow> 'a upper_pd" ("{_}\<sharp>")

translations
  "{x,xs}\<sharp>" == "{x}\<sharp> +\<sharp> {xs}\<sharp>"
  "{x}\<sharp>" == "CONST upper_unit\<cdot>x"

lemma upper_unit_Rep_compact_basis [simp]:
  "{Rep_compact_basis a}\<sharp> = upper_principal (PDUnit a)"
unfolding upper_unit_def
by (simp add: compact_basis.basis_fun_principal
    upper_principal_mono PDUnit_upper_mono)

lemma upper_plus_principal [simp]:
  "upper_principal t +\<sharp> upper_principal u = upper_principal (PDPlus t u)"
unfolding upper_plus_def
by (simp add: upper_pd.basis_fun_principal
    upper_pd.basis_fun_mono PDPlus_upper_mono)

lemma approx_upper_unit [simp]:
  "approx n\<cdot>{x}\<sharp> = {approx n\<cdot>x}\<sharp>"
apply (induct x rule: compact_basis_induct, simp)
apply (simp add: approx_Rep_compact_basis)
done

lemma approx_upper_plus [simp]:
  "approx n\<cdot>(xs +\<sharp> ys) = (approx n\<cdot>xs) +\<sharp> (approx n\<cdot>ys)"
by (induct xs ys rule: upper_principal_induct2, simp, simp, simp)

lemma upper_plus_assoc: "(xs +\<sharp> ys) +\<sharp> zs = xs +\<sharp> (ys +\<sharp> zs)"
apply (induct xs ys arbitrary: zs rule: upper_principal_induct2, simp, simp)
apply (rule_tac xs=zs in upper_principal_induct, simp)
apply (simp add: PDPlus_assoc)
done

lemma upper_plus_commute: "xs +\<sharp> ys = ys +\<sharp> xs"
apply (induct xs ys rule: upper_principal_induct2, simp, simp)
apply (simp add: PDPlus_commute)
done

lemma upper_plus_absorb: "xs +\<sharp> xs = xs"
apply (induct xs rule: upper_principal_induct, simp)
apply (simp add: PDPlus_absorb)
done

interpretation aci_upper_plus: ab_semigroup_idem_mult ["op +\<sharp>"]
  by unfold_locales
    (rule upper_plus_assoc upper_plus_commute upper_plus_absorb)+

lemma upper_plus_left_commute: "xs +\<sharp> (ys +\<sharp> zs) = ys +\<sharp> (xs +\<sharp> zs)"
by (rule aci_upper_plus.mult_left_commute)

lemma upper_plus_left_absorb: "xs +\<sharp> (xs +\<sharp> ys) = xs +\<sharp> ys"
by (rule aci_upper_plus.mult_left_idem)

lemmas upper_plus_aci = aci_upper_plus.mult_ac_idem

lemma upper_plus_less1: "xs +\<sharp> ys \<sqsubseteq> xs"
apply (induct xs ys rule: upper_principal_induct2, simp, simp)
apply (simp add: PDPlus_upper_less)
done

lemma upper_plus_less2: "xs +\<sharp> ys \<sqsubseteq> ys"
by (subst upper_plus_commute, rule upper_plus_less1)

lemma upper_plus_greatest: "\<lbrakk>xs \<sqsubseteq> ys; xs \<sqsubseteq> zs\<rbrakk> \<Longrightarrow> xs \<sqsubseteq> ys +\<sharp> zs"
apply (subst upper_plus_absorb [of xs, symmetric])
apply (erule (1) monofun_cfun [OF monofun_cfun_arg])
done

lemma upper_less_plus_iff:
  "xs \<sqsubseteq> ys +\<sharp> zs \<longleftrightarrow> xs \<sqsubseteq> ys \<and> xs \<sqsubseteq> zs"
apply safe
apply (erule trans_less [OF _ upper_plus_less1])
apply (erule trans_less [OF _ upper_plus_less2])
apply (erule (1) upper_plus_greatest)
done

lemma upper_plus_less_unit_iff:
  "xs +\<sharp> ys \<sqsubseteq> {z}\<sharp> \<longleftrightarrow> xs \<sqsubseteq> {z}\<sharp> \<or> ys \<sqsubseteq> {z}\<sharp>"
 apply (rule iffI)
  apply (subgoal_tac
    "adm (\<lambda>f. f\<cdot>xs \<sqsubseteq> f\<cdot>{z}\<sharp> \<or> f\<cdot>ys \<sqsubseteq> f\<cdot>{z}\<sharp>)")
   apply (drule admD, rule chain_approx)
    apply (drule_tac f="approx i" in monofun_cfun_arg)
    apply (cut_tac xs="approx i\<cdot>xs" in compact_imp_upper_principal, simp)
    apply (cut_tac xs="approx i\<cdot>ys" in compact_imp_upper_principal, simp)
    apply (cut_tac x="approx i\<cdot>z" in compact_imp_Rep_compact_basis, simp)
    apply (clarify, simp add: upper_le_PDPlus_PDUnit_iff)
   apply simp
  apply simp
 apply (erule disjE)
  apply (erule trans_less [OF upper_plus_less1])
 apply (erule trans_less [OF upper_plus_less2])
done

lemma upper_unit_less_iff [simp]: "{x}\<sharp> \<sqsubseteq> {y}\<sharp> \<longleftrightarrow> x \<sqsubseteq> y"
 apply (rule iffI)
  apply (rule bifinite_less_ext)
  apply (drule_tac f="approx i" in monofun_cfun_arg, simp)
  apply (cut_tac x="approx i\<cdot>x" in compact_imp_Rep_compact_basis, simp)
  apply (cut_tac x="approx i\<cdot>y" in compact_imp_Rep_compact_basis, simp)
  apply (clarify, simp add: compact_le_def)
 apply (erule monofun_cfun_arg)
done

lemmas upper_pd_less_simps =
  upper_unit_less_iff
  upper_less_plus_iff
  upper_plus_less_unit_iff

lemma upper_unit_eq_iff [simp]: "{x}\<sharp> = {y}\<sharp> \<longleftrightarrow> x = y"
unfolding po_eq_conv by simp

lemma upper_unit_strict [simp]: "{\<bottom>}\<sharp> = \<bottom>"
unfolding inst_upper_pd_pcpo Rep_compact_bot [symmetric] by simp

lemma upper_plus_strict1 [simp]: "\<bottom> +\<sharp> ys = \<bottom>"
by (rule UU_I, rule upper_plus_less1)

lemma upper_plus_strict2 [simp]: "xs +\<sharp> \<bottom> = \<bottom>"
by (rule UU_I, rule upper_plus_less2)

lemma upper_unit_strict_iff [simp]: "{x}\<sharp> = \<bottom> \<longleftrightarrow> x = \<bottom>"
unfolding upper_unit_strict [symmetric] by (rule upper_unit_eq_iff)

lemma upper_plus_strict_iff [simp]:
  "xs +\<sharp> ys = \<bottom> \<longleftrightarrow> xs = \<bottom> \<or> ys = \<bottom>"
apply (rule iffI)
apply (erule rev_mp)
apply (rule upper_principal_induct2 [where xs=xs and ys=ys], simp, simp)
apply (simp add: inst_upper_pd_pcpo upper_principal_eq_iff
                 upper_le_PDPlus_PDUnit_iff)
apply auto
done

lemma compact_upper_unit_iff [simp]: "compact {x}\<sharp> \<longleftrightarrow> compact x"
unfolding bifinite_compact_iff by simp

lemma compact_upper_plus [simp]:
  "\<lbrakk>compact xs; compact ys\<rbrakk> \<Longrightarrow> compact (xs +\<sharp> ys)"
apply (drule compact_imp_upper_principal)+
apply (auto simp add: compact_upper_principal)
done


subsection {* Induction rules *}

lemma upper_pd_induct1:
  assumes P: "adm P"
  assumes unit: "\<And>x. P {x}\<sharp>"
  assumes insert: "\<And>x ys. \<lbrakk>P {x}\<sharp>; P ys\<rbrakk> \<Longrightarrow> P ({x}\<sharp> +\<sharp> ys)"
  shows "P (xs::'a upper_pd)"
apply (induct xs rule: upper_principal_induct, rule P)
apply (induct_tac t rule: pd_basis_induct1)
apply (simp only: upper_unit_Rep_compact_basis [symmetric])
apply (rule unit)
apply (simp only: upper_unit_Rep_compact_basis [symmetric]
                  upper_plus_principal [symmetric])
apply (erule insert [OF unit])
done

lemma upper_pd_induct:
  assumes P: "adm P"
  assumes unit: "\<And>x. P {x}\<sharp>"
  assumes plus: "\<And>xs ys. \<lbrakk>P xs; P ys\<rbrakk> \<Longrightarrow> P (xs +\<sharp> ys)"
  shows "P (xs::'a upper_pd)"
apply (induct xs rule: upper_principal_induct, rule P)
apply (induct_tac t rule: pd_basis_induct)
apply (simp only: upper_unit_Rep_compact_basis [symmetric] unit)
apply (simp only: upper_plus_principal [symmetric] plus)
done


subsection {* Monadic bind *}

definition
  upper_bind_basis ::
  "'a pd_basis \<Rightarrow> ('a \<rightarrow> 'b upper_pd) \<rightarrow> 'b upper_pd" where
  "upper_bind_basis = fold_pd
    (\<lambda>a. \<Lambda> f. f\<cdot>(Rep_compact_basis a))
    (\<lambda>x y. \<Lambda> f. x\<cdot>f +\<sharp> y\<cdot>f)"

lemma ACI_upper_bind:
  "ab_semigroup_idem_mult (\<lambda>x y. \<Lambda> f. x\<cdot>f +\<sharp> y\<cdot>f)"
apply unfold_locales
apply (simp add: upper_plus_assoc)
apply (simp add: upper_plus_commute)
apply (simp add: upper_plus_absorb eta_cfun)
done

lemma upper_bind_basis_simps [simp]:
  "upper_bind_basis (PDUnit a) =
    (\<Lambda> f. f\<cdot>(Rep_compact_basis a))"
  "upper_bind_basis (PDPlus t u) =
    (\<Lambda> f. upper_bind_basis t\<cdot>f +\<sharp> upper_bind_basis u\<cdot>f)"
unfolding upper_bind_basis_def
apply -
apply (rule fold_pd_PDUnit [OF ACI_upper_bind])
apply (rule fold_pd_PDPlus [OF ACI_upper_bind])
done

lemma upper_bind_basis_mono:
  "t \<le>\<sharp> u \<Longrightarrow> upper_bind_basis t \<sqsubseteq> upper_bind_basis u"
unfolding expand_cfun_less
apply (erule upper_le_induct, safe)
apply (simp add: compact_le_def monofun_cfun)
apply (simp add: trans_less [OF upper_plus_less1])
apply (simp add: upper_less_plus_iff)
done

definition
  upper_bind :: "'a upper_pd \<rightarrow> ('a \<rightarrow> 'b upper_pd) \<rightarrow> 'b upper_pd" where
  "upper_bind = upper_pd.basis_fun upper_bind_basis"

lemma upper_bind_principal [simp]:
  "upper_bind\<cdot>(upper_principal t) = upper_bind_basis t"
unfolding upper_bind_def
apply (rule upper_pd.basis_fun_principal)
apply (erule upper_bind_basis_mono)
done

lemma upper_bind_unit [simp]:
  "upper_bind\<cdot>{x}\<sharp>\<cdot>f = f\<cdot>x"
by (induct x rule: compact_basis_induct, simp, simp)

lemma upper_bind_plus [simp]:
  "upper_bind\<cdot>(xs +\<sharp> ys)\<cdot>f = upper_bind\<cdot>xs\<cdot>f +\<sharp> upper_bind\<cdot>ys\<cdot>f"
by (induct xs ys rule: upper_principal_induct2, simp, simp, simp)

lemma upper_bind_strict [simp]: "upper_bind\<cdot>\<bottom>\<cdot>f = f\<cdot>\<bottom>"
unfolding upper_unit_strict [symmetric] by (rule upper_bind_unit)


subsection {* Map and join *}

definition
  upper_map :: "('a \<rightarrow> 'b) \<rightarrow> 'a upper_pd \<rightarrow> 'b upper_pd" where
  "upper_map = (\<Lambda> f xs. upper_bind\<cdot>xs\<cdot>(\<Lambda> x. {f\<cdot>x}\<sharp>))"

definition
  upper_join :: "'a upper_pd upper_pd \<rightarrow> 'a upper_pd" where
  "upper_join = (\<Lambda> xss. upper_bind\<cdot>xss\<cdot>(\<Lambda> xs. xs))"

lemma upper_map_unit [simp]:
  "upper_map\<cdot>f\<cdot>{x}\<sharp> = {f\<cdot>x}\<sharp>"
unfolding upper_map_def by simp

lemma upper_map_plus [simp]:
  "upper_map\<cdot>f\<cdot>(xs +\<sharp> ys) = upper_map\<cdot>f\<cdot>xs +\<sharp> upper_map\<cdot>f\<cdot>ys"
unfolding upper_map_def by simp

lemma upper_join_unit [simp]:
  "upper_join\<cdot>{xs}\<sharp> = xs"
unfolding upper_join_def by simp

lemma upper_join_plus [simp]:
  "upper_join\<cdot>(xss +\<sharp> yss) = upper_join\<cdot>xss +\<sharp> upper_join\<cdot>yss"
unfolding upper_join_def by simp

lemma upper_map_ident: "upper_map\<cdot>(\<Lambda> x. x)\<cdot>xs = xs"
by (induct xs rule: upper_pd_induct, simp_all)

lemma upper_map_map:
  "upper_map\<cdot>f\<cdot>(upper_map\<cdot>g\<cdot>xs) = upper_map\<cdot>(\<Lambda> x. f\<cdot>(g\<cdot>x))\<cdot>xs"
by (induct xs rule: upper_pd_induct, simp_all)

lemma upper_join_map_unit:
  "upper_join\<cdot>(upper_map\<cdot>upper_unit\<cdot>xs) = xs"
by (induct xs rule: upper_pd_induct, simp_all)

lemma upper_join_map_join:
  "upper_join\<cdot>(upper_map\<cdot>upper_join\<cdot>xsss) = upper_join\<cdot>(upper_join\<cdot>xsss)"
by (induct xsss rule: upper_pd_induct, simp_all)

lemma upper_join_map_map:
  "upper_join\<cdot>(upper_map\<cdot>(upper_map\<cdot>f)\<cdot>xss) =
   upper_map\<cdot>f\<cdot>(upper_join\<cdot>xss)"
by (induct xss rule: upper_pd_induct, simp_all)

lemma upper_map_approx: "upper_map\<cdot>(approx n)\<cdot>xs = approx n\<cdot>xs"
by (induct xs rule: upper_pd_induct, simp_all)

end
