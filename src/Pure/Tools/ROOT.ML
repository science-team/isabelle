(*  Title:      Pure/Tools/ROOT.ML
    ID:         $Id: ROOT.ML,v 1.29 2007/12/04 21:49:26 wenzelm Exp $

Miscellaneous tools and packages for Pure Isabelle.
*)

use "named_thms.ML";
use "isabelle_process.ML";

(*basic XML support*)
use "xml_syntax.ML";

(*derived theory and proof elements*)
use "invoke.ML";

